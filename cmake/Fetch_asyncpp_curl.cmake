include(FetchContent)

FetchContent_Declare(
    asyncpp_curl
    GIT_REPOSITORY "git@gitlab.com:Thalhammer/asyncpp-curl.git"
)
FetchContent_MakeAvailable(asyncpp_curl)