include(FetchContent)

set(JWT_BUILD_EXAMPLES OFF CACHE INTERNAL "Disable jwt examples")
FetchContent_Declare(
    jwt-cpp
    GIT_REPOSITORY "https://github.com/Thalhammer/jwt-cpp"
)
FetchContent_MakeAvailable(jwt-cpp)