include(FetchContent)

FetchContent_Declare(
    asyncpp_grpc
    GIT_REPOSITORY "git@gitlab.com:Thalhammer/asyncpp-grpc.git"
)
FetchContent_MakeAvailable(asyncpp_grpc)