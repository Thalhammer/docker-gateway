# Build: docker build -t registry.gitlab.com/cyberpunks1/medius-server-impl/warhawk:latest . -f docker/Dockerfile.alpine-build
# Push: docker push registry.gitlab.com/cyberpunks1/medius-server-impl/warhawk:latest

FROM registry.gitlab.com/cyberpunks1/medius-server-impl/alpine-ci-container-new:latest as build
WORKDIR /src
COPY / /src/
RUN rm -rf build \
    && mkdir build \
    && cd build \
    && cmake .. -DCMAKE_BUILD_TYPE=Release -DBUILD_TEST=OFF -DWITH_ASAN=OFF \
    && make -j$(nproc)

FROM alpine:latest
RUN apk add --update --no-cache libstdc++ libgcc
WORKDIR /opt/docker_gateway/
COPY --from=build /src/build/gateway/docker_gateway ./server
CMD ["./server"]
