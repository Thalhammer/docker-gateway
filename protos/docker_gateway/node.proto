syntax = "proto3";
package it.thalhammer.docker_gateway;
import "options.proto";
import "swarm.proto";
import "system.proto";

message NodeSpec {
	string name = 1 [ (docker_json_name) = "Name" ];
	map<string, string> labels = 2 [ (docker_json_name) = "Labels" ];
	string role = 3 [ (docker_json_name) = "Role" ];
	string availability = 4 [ (docker_json_name) = "Availability" ];
}

message Platform {
	string architecture = 1;
	string os = 2;
}

message EngineDescription {
	message PluginEntry {
		string type = 1;
		string name = 2;
	}
	string engine_version = 1;
	map<string, string> labels = 2;
	repeated PluginEntry plugins = 3;
}

message ResourceObject {
	int64 nano_cpus = 1 [ (docker_json_name) = "NanoCPUs" ];
	int64 memory_bytes = 2 [ (docker_json_name) = "MemoryBytes" ];
	repeated GenericResource generic_resources = 3 [ (docker_json_name) = "GenericResources" ];
}

message NodeDescription {
	string hostname = 1;
	Platform platform = 2;
	ResourceObject resources = 3;
	EngineDescription engine = 4;
	TLSInfo tls_info = 5;
}

message NodeStatus {
	enum NodeState {
		unknown = 0;
		down = 1;
		ready = 2;
		disconnected = 3;
	}

	NodeState state = 1;
	string message = 2;
	string addr = 3;
}

message ManagerStatus {
	enum Reachability {
		unknown = 0;
		unreachable = 1;
		reachable = 2;
	}

	bool leader = 1;
	Reachability reachability = 2;
	string addr = 3;
}

message Node {
	string id = 1;
	ObjectVersion version = 2;
	string created_at = 3;
	string updated_at = 4;
	NodeSpec spec = 5;
	NodeDescription description = 6;
	NodeStatus status = 7;
	ManagerStatus manager_status = 8;
}

message NodeListRequest { repeated string filters = 1; }

message NodeListResponse { repeated Node nodes = 1 [ (docker_json_inline) = true ]; }

message NodeInspectRequest { string id = 1; }

message NodeInspectResponse { Node node = 1 [ (docker_json_inline) = true ]; }

message NodeDeleteRequest {
	string id = 1;
	bool force = 2;
}

message NodeDeleteResponse {}

message NodeUpdateRequest {
	string id = 1;
	int64 version = 2;
	NodeSpec spec = 3;
}

message NodeUpdateResponse {}