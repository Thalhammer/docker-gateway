syntax = "proto3";
package it.thalhammer.docker_gateway;
import "options.proto";
import "registry.proto";

message ContainerConfig {
	message ExposedPort {}
	message Volume {}
	string hostname = 1 [ (docker_json_name) = "Hostname" ];
	string domainname = 2 [ (docker_json_name) = "Domainname" ];
	string user = 3 [ (docker_json_name) = "User" ];
	bool attach_stdin = 4 [ (docker_json_name) = "AttachStdin" ];
	bool attach_stdout = 5 [ (docker_json_name) = "AttachStdout" ];
	bool attach_stderr = 6 [ (docker_json_name) = "AttachStderr" ];
	map<string, ExposedPort> exposed_ports = 7 [ (docker_json_name) = "ExposedPorts" ];
	bool tty = 8 [ (docker_json_name) = "Tty" ];
	bool open_stdin = 9 [ (docker_json_name) = "OpenStdin" ];
	bool stdin_once = 10 [ (docker_json_name) = "StdinOnce" ];
	repeated string env = 11 [ (docker_json_name) = "Env" ];
	repeated string cmd = 12 [ (docker_json_name) = "Cmd" ];
	HealthConfig health_check = 13 [ (docker_json_name) = "Healthcheck" ];
	bool args_escaped = 14 [ (docker_json_name) = "ArgsEscaped" ];
	string image = 15 [ (docker_json_name) = "Image" ];
	map<string, Volume> volumes = 16 [ (docker_json_name) = "Volumes" ];
	string working_dir = 17 [ (docker_json_name) = "WorkingDir" ];
	repeated string entrypoint = 18 [ (docker_json_name) = "Entrypoint" ];
	bool network_disable = 19 [ (docker_json_name) = "NetworkDisabled" ];
	string mac_address = 20 [ (docker_json_name) = "MacAddress" ];
	repeated string on_build = 21 [ (docker_json_name) = "OnBuild" ];
	map<string, string> labels = 22 [ (docker_json_name) = "Labels" ];
	string stop_signal = 23 [ (docker_json_name) = "StopSignal" ];
	uint32 stop_timeout = 24 [ (docker_json_name) = "StopTimeout" ];
	repeated string shell = 25 [ (docker_json_name) = "Shell" ];
}

message HealthConfig {
	repeated string test = 1 [ (docker_json_name) = "Test" ];
	int64 interval = 2 [ (docker_json_name) = "Interval" ];
	int64 timeout = 3 [ (docker_json_name) = "Timeout" ];
	int64 retries = 4 [ (docker_json_name) = "Retries" ];
	int64 start_period = 5 [ (docker_json_name) = "StartPeriod" ];
}

message GraphDriverData {
	string name = 1;
	map<string, string> data = 2;
}

message ImageSummary {
	string id = 1;
	string parent_id = 2;
	repeated string repo_tags = 3;
	repeated string repo_digests = 4;
	int64 created = 5;
	int64 size = 6;
	int64 shared_size = 7;
	int64 virtual_size = 8;
	map<string, string> labels = 9;
	int64 containers = 10;
}

message Image {
	message RootFS {
		string type = 1;
		repeated string layers = 2;
		string base_layer = 3;
	}
	message Metadata { string last_tag_time = 1; }
	string id = 1;
	repeated string repo_tags = 2;
	repeated string repo_digests = 3;
	string parent = 4;
	string comment = 5;
	string created = 6;
	string container = 7;
	ContainerConfig container_config = 8;
	string docker_version = 9;
	string author = 10;
	ContainerConfig config = 11;
	string architecture = 12;
	string os = 13;
	string os_version = 14;
	int64 size = 15;
	int64 virtual_size = 16;
	GraphDriverData graph_driver = 17;
	RootFS root_fs = 18;
	Metadata metadata = 19;
}

message ImageListRequest {
	bool all = 1;
	repeated string filters = 2;
	bool digests = 3;
}

message ImageListResponse { repeated ImageSummary images = 1 [ (docker_json_inline) = true ]; }

message ImageBuildRequest {
	message Metadata {
		string dockerfile = 1;
		string tag = 2;
		string extra_hosts = 3;
		string remote = 4;
		bool quiet = 5;
		bool nocache = 6;
		string cachefrom = 7;
		bool pull = 8;
		bool rm = 9;
		bool force_rm = 10;
		uint64 memory = 11;
		uint64 memory_swap = 12;
		uint64 cpu_shares = 13;
		string cpu_setcpus = 14;
		uint64 cpu_period = 15;
		uint64 cpu_quota = 16;
		map<string, string> build_args = 17;
		uint64 shm_size = 18;
		bool squash = 19;
		map<string, string> labels = 20;
		string networkmode = 21;
		string platform = 22;
		string target = 23;
		string outputs = 24;
	}
	Metadata metadata = 1;
	bytes chunk = 2;
}

message ImageBuildResponse {
	string stream = 1;
	string status = 2;
	map<string, string> aux = 3;
	map<string, string> error_details = 4;
}

message ImageBuildPruneRequest {
	int64 keep_storage = 1;
	bool all = 2;
	repeated string filters = 3;
}

message ImageBuildPruneResponse {
	repeated string caches_deleted = 1;
	int64 space_reclaimed = 2;
}

message ImageInspectRequest { string name = 1; }

message ImageInspectResponse { Image image = 1 [ (docker_json_inline) = true ]; }

message ImagePullRequest {
	string name = 1;
	string tag = 2;
	string platform = 3;
	RegistryAuthentication registry_auth = 4;
}

message ImagePullResponse {
	message Details {
		uint64 current = 1;
		uint64 total = 2;
	}
	string status = 1;
	string id = 2;
	string progress = 3;
	Details progress_detail = 4;
}

message ImageHistoryRequest { string name = 1; }

message ImageHistoryResponse {
	message HistoryResponseItem {
		string id = 1;
		int64 created = 2;
		string created_by = 3;
		repeated string tags = 4;
		int64 size = 5;
		string comment = 6;
	}
	repeated HistoryResponseItem history = 1 [ (docker_json_inline) = true ];
}

message ImagePushRequest {
	string name = 1;
	string tag = 2;
	RegistryAuthentication registry_auth = 3;
}

message ImagePushResponse {
	message Details {
		uint64 current = 1;
		uint64 total = 2;
	}
	string status = 1;
	string id = 2;
	string progress = 3;
	Details progress_detail = 4;
}

message ImageTagRequest {
	string name = 1;
	string repo = 2;
	string tag = 3;
}

message ImageTagResponse {}

message ImageDeleteRequest {
	string name = 1;
	bool force = 2;
	bool noprune = 3;
}

message ImageDeleteResponseItem {
	string untagged = 1;
	string deleted = 2;
}

message ImageDeleteResponse { repeated ImageDeleteResponseItem items = 1 [ (docker_json_inline) = true ]; }

message ImageSearchRequest {
	string term = 1;
	int64 limit = 2;
	repeated string filters = 3;
}

message ImageSearchResponse {
	message Item {
		string description = 1;
		bool is_official = 2;
		bool is_automated = 3;
		string name = 4;
		int64 star_count = 5;
	}
	repeated Item items = 1 [ (docker_json_inline) = true ];
}

message ImagePruneRequest { repeated string filters = 1; }

message ImagePruneResponse {
	repeated ImageDeleteResponseItem images_deleted = 1;
	int64 space_reclaimed = 2;
}

message ImageCommitRequest {
	string container = 1;
	string repo = 2;
	string tag = 3;
	string comment = 4;
	string author = 5;
	bool pause = 6;
	string changes = 7;
	ContainerConfig config = 8;
}

message ImageCommitResponse { string id = 1; }

message ImageGetRequest {
	string name = 1;
	uint32 chunk_size = 2;
}

message ImageGetResponse { bytes chunk = 1; }

message ImageGetAllRequest {
	repeated string names = 1;
	uint32 chunk_size = 2;
}

message ImageGetAllResponse { bytes chunk = 1; }

message ImageLoadRequest { bytes chunk = 1; }

message ImageLoadResponse { string stream = 1; }