# Protobuf definition cleaner
Parses the specified definition file, inlines all imports within the same package, sorts all definitions and outputs
a consistently formatted merger of the input files, removing all options. This allows generating a single distribution proto file
which does not contain implementation details (the docker json mapping) and is nicely formatted without having to maintain two
protobuf files.