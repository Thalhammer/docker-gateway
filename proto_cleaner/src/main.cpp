#include <fstream>
#include <google/protobuf/compiler/importer.h>
#include <google/protobuf/compiler/parser.h>
#include <spdlog/spdlog.h>

using namespace google::protobuf;
using namespace google::protobuf::io;
using namespace google::protobuf::compiler;

class spdlog_error_reporter : public MultiFileErrorCollector {
public:
	// Line and column numbers are zero-based.  A line number of -1 indicates
	// an error with the entire file (e.g. "not found").
	void AddError(const std::string& filename, int line, int column, const std::string& message) override {
		spdlog::error("{}:{}:{} {}", filename, line, column, message);
	}

	void AddWarning(const std::string& filename, int line, int column, const std::string& message) override {
		spdlog::warn("{}:{}:{} {}", filename, line, column, message);
	}
};

std::vector<const ServiceDescriptor*> get_services(const FileDescriptor* file) {
	std::vector<const ServiceDescriptor*> res;
	for (int i = 0; i < file->service_count(); i++) {
		res.push_back(file->service(i));
	}
	for (int i = 0; i < file->dependency_count(); i++) {
		auto dep = get_services(file->dependency(i));
		auto s = res.size();
		res.resize(s + dep.size());
		std::copy_n(dep.begin(), dep.size(), res.begin() + s);
	}
	return res;
}

std::vector<const Descriptor*> get_messages(const FileDescriptor* file) {
	std::vector<const Descriptor*> res;
	for (int i = 0; i < file->message_type_count(); i++) {
		res.push_back(file->message_type(i));
	}
	for (int i = 0; i < file->dependency_count(); i++) {
		auto dep = get_messages(file->dependency(i));
		auto s = res.size();
		res.resize(s + dep.size());
		std::copy_n(dep.begin(), dep.size(), res.begin() + s);
	}
	return res;
}

std::vector<const FileDescriptor*> get_dependencies(const FileDescriptor* file) {
	std::vector<const FileDescriptor*> res;
	for (int i = 0; i < file->dependency_count(); i++) {
		res.push_back(file->dependency(i));
	}
	for (int i = 0; i < file->dependency_count(); i++) {
		auto dep = get_dependencies(file->dependency(i));
		auto s = res.size();
		res.resize(s + dep.size());
		std::copy_n(dep.begin(), dep.size(), res.begin() + s);
	}
	return res;
}

std::vector<const EnumDescriptor*> get_enums(const FileDescriptor* file) {
	std::vector<const EnumDescriptor*> res;
	for (int i = 0; i < file->enum_type_count(); i++) {
		res.push_back(file->enum_type(i));
	}
	for (int i = 0; i < file->dependency_count(); i++) {
		auto dep = get_enums(file->dependency(i));
		auto s = res.size();
		res.resize(s + dep.size());
		std::copy_n(dep.begin(), dep.size(), res.begin() + s);
	}
	return res;
}

void print_field(std::ostream& out, const FieldDescriptor* e, size_t indent = 0) {
	std::string prefix;
	prefix.resize(indent, '\t');
	std::string type = e->type_name();
	if (e->type() == FieldDescriptor::TYPE_MESSAGE)
		type = e->message_type()->name();
	else if (e->type() == FieldDescriptor::TYPE_ENUM)
		type = e->enum_type()->name();
	out << prefix << (e->label() == FieldDescriptor::LABEL_REPEATED ? "repeated " : "") << type << " " << e->name() << " = " << e->number() << ";\n";
}

void print_enum(std::ostream& out, const EnumDescriptor* e, size_t indent = 0) {
	std::string prefix;
	prefix.resize(indent, '\t');
	out << prefix << "enum " << e->name() << " {\n";
	for (int i = 0; i < e->value_count(); i++) {
		auto v = e->value(i);
		out << prefix << "\t" << v->name() << " = " << v->number() << ";\n";
	}
	out << prefix << "}\n\n";
}

void print_message(std::ostream& out, const Descriptor* e, size_t indent = 0) {
	std::string prefix;
	prefix.resize(indent, '\t');
	out << prefix << "message " << e->name() << " {\n";
	for (int i = 0; i < e->nested_type_count(); i++) {
		print_message(out, e->nested_type(i), indent + 1);
	}
	for (int i = 0; i < e->enum_type_count(); i++) {
		print_enum(out, e->enum_type(i), indent + 1);
	}
	for (int i = 0; i < e->field_count(); i++) {
		print_field(out, e->field(i), indent + 1);
	}
	out << prefix << "}\n\n";
}

void print_service(std::ostream& out, const ServiceDescriptor* e, size_t indent = 0) {
	std::string prefix;
	prefix.resize(indent, '\t');
	out << prefix << "service " << e->name() << " {\n";
	for (int i = 0; i < e->method_count(); i++) {
		auto m = e->method(i);
		out << prefix << "\trpc " << m->name() << "(";
		if (m->client_streaming()) out << "stream ";
		out << m->input_type()->name() << ") returns (";
		if (m->server_streaming()) out << "stream ";
		out << m->output_type()->name() << ");\n";
	}
	out << prefix << "}\n\n";
}

struct options {
	std::string proto_file;
	std::string output_file;
	std::vector<std::string> include_dirs;

	bool parse(int argc, const char** argv) {
		if (argc < 2) return false;
		for (int i = 0; i < argc; i++) {
			std::string arg{argv[i]};
			if (arg == "-I") {
				i++;
				if (argc <= i) return false;
				include_dirs.push_back(argv[i]);
			} else if (proto_file.empty()) {
				proto_file = std::move(arg);
			} else if (output_file.empty()) {
				output_file = std::move(arg);
			} else {
				return false;
			}
		}
		return !proto_file.empty() && !output_file.empty();
	}
};

int main(int argc, const char** argv) {
	options opts{};
	if (!opts.parse(argc - 1, argv + 1)) {
		spdlog::error("Usage: {} [options] proto-file output", argv[0]);
		return -1;
	}

	spdlog::info("in = '{}' out = '{}'", opts.proto_file, opts.output_file);
	for (auto& e : opts.include_dirs)
		spdlog::info("include = '{}'", e);

	spdlog_error_reporter reporter;
	DiskSourceTree source_tree;
	for (auto& e : opts.include_dirs)
		source_tree.MapPath("", e);
	Importer parser{&source_tree, &reporter};

	const google::protobuf::FileDescriptor* file_desc = parser.Import(opts.proto_file);
	if (file_desc == NULL) {
		spdlog::error("Cannot get file descriptor from file descriptor proto");
		return -1;
	}

	auto services = get_services(file_desc);
	auto messages = get_messages(file_desc);
	auto enums = get_enums(file_desc);
	auto files = get_dependencies(file_desc);

	std::sort(services.begin(), services.end(),
			  [](const ServiceDescriptor* a, const ServiceDescriptor* b) { return std::less<std::string>{}(a->full_name(), b->full_name()); });
	std::sort(messages.begin(), messages.end(),
			  [](const Descriptor* a, const Descriptor* b) { return std::less<std::string>{}(a->full_name(), b->full_name()); });
	std::sort(enums.begin(), enums.end(),
			  [](const EnumDescriptor* a, const EnumDescriptor* b) { return std::less<std::string>{}(a->full_name(), b->full_name()); });
	std::sort(files.begin(), files.end(), [](const FileDescriptor* a, const FileDescriptor* b) { return std::less<std::string>{}(a->name(), b->name()); });
	services.erase(
		std::unique(services.begin(), services.end(), [](const ServiceDescriptor* a, const ServiceDescriptor* b) { return a->full_name() == b->full_name(); }),
		services.end());
	messages.erase(std::unique(messages.begin(), messages.end(), [](const Descriptor* a, const Descriptor* b) { return a->full_name() == b->full_name(); }),
				   messages.end());
	enums.erase(std::unique(enums.begin(), enums.end(), [](const EnumDescriptor* a, const EnumDescriptor* b) { return a->full_name() == b->full_name(); }),
				enums.end());
	files.erase(std::unique(files.begin(), files.end(), [](const FileDescriptor* a, const FileDescriptor* b) { return a->name() == b->name(); }), files.end());

	auto main_package = file_desc->package();
	std::erase_if(services, [&main_package](const ServiceDescriptor* a) { return a->file()->package() != main_package; });
	std::erase_if(messages, [&main_package](const Descriptor* a) { return a->file()->package() != main_package; });
	std::erase_if(enums, [&main_package](const EnumDescriptor* a) { return a->file()->package() != main_package; });
	std::erase_if(files, [&main_package](const FileDescriptor* a) { return a->package() == main_package; });

	spdlog::info("Merging {} services, and {} messages with {} imports", services.size(), messages.size(), files.size());

	//bool remove_options = true;

	std::ofstream out{opts.output_file, std::ios::binary | std::ios::trunc};
	if (!out) {
		spdlog::error("failed to open output file");
		return -1;
	}

	out << "syntax = \"proto3\";\n\n";
	for (auto e : files) {
		out << "import \"" << e->name() << "\";\n";
	}

	if (!file_desc->package().empty()) out << "\npackage " << file_desc->package() << ";\n";

	out << "\n";

	for (auto e : services) {
		print_service(out, e);
	}

	for (auto e : messages) {
		print_message(out, e);
	}

	for (auto e : enums) {
		print_enum(out, e);
	}

	return 0;
}