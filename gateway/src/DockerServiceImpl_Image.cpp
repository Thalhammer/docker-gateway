#include "DockerServiceImpl.h"
#include <async_helpers.h>
#include <asyncpp/curl/exception.h>
#include <asyncpp/curl/executor.h>
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <curl/curl.h>
#include <json_map.h>
#include <regex>
#include <spdlog/spdlog.h>

using namespace asyncpp;

namespace it::thalhammer::docker_gateway {
	using namespace ::google::protobuf;

	namespace {

		CALL(ImageListCallData, RequestImageList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/images/json?filters={}&all={}&digest={}", e(filters.dump()), req.all(), req.digests());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		class ImageBuildCallData : public asyncpp::grpc::calldata_interface {
		public:
			ImageBuildCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_stream{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_stream.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else
						m_stream.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
				m_hdl.set_readfunction([this](char* ptr, size_t size) -> size_t {
					auto c = m_request.mutable_chunk();
					if (!c->empty()) {
						size = std::min(c->size(), size);
						memcpy(ptr, c->data(), size);
						c->erase(0, size);
						return size;
					} else {
						if (m_read_done) return 0;
						m_stream.Read(&m_request, ptr_tag<tag::read>(this));
						return CURL_READFUNC_PAUSE;
					}
				});
				m_hdl.set_option_bool(CURLOPT_POST, true);
				m_headers.append("Content-Type: application/x-tar");
				m_hdl.set_headers(m_headers);
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (!m_write_done) { return CURL_WRITEFUNC_PAUSE; }
				m_write_buf.append(data, size);
				auto pos = m_write_buf.find_first_of("\r\n");
				if (pos != std::string::npos) {
					process_line(std::string_view{m_write_buf.c_str(), pos});
					auto end = m_write_buf.find_first_not_of("\r\n", pos);
					m_write_buf.erase(0, end);
				}
				return size;
			}

			void process_line(std::string_view view) {
				try {
					auto j = nlohmann::json::parse(view);
					if (j.dump() == m_last_status) return;
					m_last_status = j.dump();
					m_response.Clear();
					json_map::from_json(j, m_response);
					m_write_done = false;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
				} catch (...) { spdlog::error("ImageBuildCallData({}) failed to parse json: \"{}\"", static_cast<void*>(this), view); }
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestImageBuild(&m_ctx, &m_stream, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ImageBuildCallData(m_service, m_cq))->start();
					m_stream.Read(&m_request, ptr_tag<tag::read>(this));
					break;
				}
				case tag::read: {
					if (m_running) {
						if (!ok) m_read_done = true;
						m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_SEND); });
					} else {
						if (!m_request.has_metadata()) {
							return m_stream.Finish(::grpc::Status(::grpc::StatusCode::INVALID_ARGUMENT, "Metadata is missing"), ptr_tag<tag::finish>(this));
						}
						auto& meta = m_request.metadata();
						nlohmann::json build_args = nlohmann::json::object();
						for (auto& e : meta.build_args())
							build_args[e.first] = e.second;
						nlohmann::json labels = nlohmann::json::object();
						for (auto& e : meta.labels())
							labels[e.first] = e.second;
						auto url = fmt::format("/v1.41/build?dockerfile={}&t={}&remote={}&q={}&nocache={}&cachefrom={}&pull={}&rm={}&forcerm={}",
											   e(meta.dockerfile()), e(meta.tag()), e(meta.remote()), meta.quiet(), meta.nocache(), e(meta.cachefrom()),
											   meta.pull(), meta.rm(), meta.force_rm());
						url += fmt::format("&memory={}&memswap={}&cpushares={}&cpusetcpus={}&cpuperiod={}&cpuquota={}&buildargs={}&shmsize={}&squash={}",
										   meta.memory(), meta.memory_swap(), meta.cpu_shares(), e(meta.cpu_setcpus()), meta.cpu_period(), meta.cpu_quota(),
										   e(build_args.dump()), meta.shm_size(), meta.squash());
						url += fmt::format("&labels={}&networkmode={}&platform={}&target={}&outputs={}", e(labels.dump()), e(meta.networkmode()),
										   e(meta.platform()), e(meta.target()), e(meta.outputs()));
						if (!meta.extra_hosts().empty()) url += "&extrahosts=" + e(meta.extra_hosts());
						m_service->get_client().prepare_handle(m_hdl, url);
						m_service->get_client().get_executor().add_handle(m_hdl);
					}
					m_running = true;
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_done == false);
						m_write_done = true;
					}
					m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_RECV); });
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ImageBuildRequest m_request{};
			ImageBuildResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncReaderWriter<ImageBuildResponse, ImageBuildRequest> m_stream;
			// Safety checking
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write, read };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_headers;
			std::mutex m_write_mtx{};
			bool m_running{false};
			bool m_write_done{true};
			bool m_read_done{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};
		};

		CALL(ImageBuildPruneCallData, RequestImageBuildPrune, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res =
				co_await client.post_json("/v1.41/build/prune?filters={}&all={}&keep-storage={}", no_body, e(filters.dump()), req.all(), req.keep_storage());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ImageInspectCallData, RequestImageInspect, {
			auto res = co_await client.get_json("/v1.41/images/{}/json", req.name());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		class ImagePullCallData : public asyncpp::grpc::calldata_interface {
		public:
			ImagePullCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_responder{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_responder.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else
						m_responder.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (!m_write_done) {
					m_write_curl_paused = true;
					return CURL_WRITEFUNC_PAUSE;
				}
				m_write_buf.append(data, size);
				auto pos = m_write_buf.find_first_of("\r\n");
				if (pos != std::string::npos) {
					process_line(std::string_view{m_write_buf.c_str(), pos});
					auto end = m_write_buf.find_first_not_of("\r\n", pos);
					m_write_buf.erase(0, end);
				}
				return size;
			}

			void process_line(std::string_view view) {
				try {
					auto j = nlohmann::json::parse(view);
					if (j.dump() == m_last_status) return;
					m_last_status = j.dump();
					m_response.Clear();
					json_map::from_json(j, m_response);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
				} catch (...) { spdlog::error("ImagePullCallData({}) failed to parse json: \"{}\"", static_cast<void*>(this), view); }
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestImagePull(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ImagePullCallData(m_service, m_cq))->start();
					m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/images/create?fromImage={}&tag={}&platform={}", e(m_request.name()),
																			  e(m_request.tag()), e(m_request.platform())));
					m_hdl.set_option_bool(CURLOPT_POST, true);
					m_hdl.set_option_long(CURLOPT_POSTFIELDSIZE, 0);
					if (m_request.has_registry_auth()) {
						m_hdl_headers.append(("X-Registry-Auth: " + DockerServiceImpl::build_registry_auth(m_request.registry_auth())).c_str());
						m_hdl.set_headers(m_hdl_headers);
					}
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_done == false);
						m_write_done = true;
					}
					if (m_write_curl_paused) {
						m_service->get_client().get_executor().push([&]() {
							m_write_curl_paused = false;
							m_hdl.unpause(CURLPAUSE_RECV);
						});
					}
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ImagePullRequest m_request{};
			ImagePullResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncWriter<ImagePullResponse> m_responder;
			// Safety checking
			//bool m_finish_called{false};
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_hdl_headers{};
			std::mutex m_write_mtx{};
			bool m_write_done{true};
			bool m_write_curl_paused{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};
		};

		CALL(ImageHistoryCallData, RequestImageHistory, {
			auto res = co_await client.get_json("/v1.41/images/{}/history", req.name());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		class ImagePushCallData : public asyncpp::grpc::calldata_interface {
		public:
			ImagePushCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_responder{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						spdlog::info("ImagePushCallData done {} {}", m_hdl.get_response_code(), m_error_buf);
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_responder.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else
						m_responder.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (!m_write_done) {
					m_write_curl_paused = true;
					return CURL_WRITEFUNC_PAUSE;
				}
				m_write_buf.append(data, size);
				auto pos = m_write_buf.find_first_of("\r\n");
				if (pos != std::string::npos) {
					process_line(std::string_view{m_write_buf.c_str(), pos});
					auto end = m_write_buf.find_first_not_of("\r\n", pos);
					m_write_buf.erase(0, end);
				}
				return size;
			}

			void process_line(std::string_view view) {
				try {
					auto j = nlohmann::json::parse(view);
					if (j.dump() == m_last_status) return;
					m_last_status = j.dump();
					m_response.Clear();
					json_map::from_json(j, m_response);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
				} catch (...) { spdlog::error("ImagePushCallData({}) failed to parse json: \"{}\"", static_cast<void*>(this), view); }
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestImagePush(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ImagePushCallData(m_service, m_cq))->start();
					m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/images/{}/push?tag={}", m_request.name(), e(m_request.tag())));
					m_hdl.set_option_bool(CURLOPT_POST, true);
					m_hdl.set_option_long(CURLOPT_POSTFIELDSIZE, 0);
					if (m_request.has_registry_auth()) {
						m_hdl_headers.append(("X-Registry-Auth: " + DockerServiceImpl::build_registry_auth(m_request.registry_auth())).c_str());
						m_hdl.set_headers(m_hdl_headers);
					}
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_done == false);
						m_write_done = true;
					}
					if (m_write_curl_paused) {
						m_service->get_client().get_executor().push([&]() {
							m_write_curl_paused = false;
							m_hdl.unpause(CURLPAUSE_RECV);
						});
					}
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ImagePushRequest m_request{};
			ImagePushResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncWriter<ImagePushResponse> m_responder;
			// Safety checking
			//bool m_finish_called{false};
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_hdl_headers{};
			std::mutex m_write_mtx{};
			bool m_write_done{true};
			bool m_write_curl_paused{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};
		};

		CALL(ImageTagCallData, RequestImageTag, {
			co_await client.post_json("/v1.41/images/{}/tag?repo={}&tag={}", no_body, req.name(), req.repo(), req.tag());
			co_return ::grpc::Status::OK;
		})

		CALL(ImageDeleteCallData, RequestImageDelete, {
			auto res = co_await client.delete_json("/v1.41/images/{}?force={}&noprune={}", req.name(), req.force(), req.noprune());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ImageSearchCallData, RequestImageSearch, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/images/search?term={}&limit={}&filters={}", e(req.term()), req.limit(), e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ImagePruneCallData, RequestImagePrune, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.post_json("/v1.41/images/prune?filters={}", no_body, e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ImageCommitCallData, RequestImageCommit, {
			auto changes = req.changes().empty() ? "" : ("&changes=" + e(req.changes()));
			auto res = co_await client.post_json("/v1.41/commit?container={}&repo={}&tag={}&comment={}&author={}&pause={}{}", json_map::to_json(req.config()),
												 e(req.container()), e(req.repo()), e(req.tag()), e(req.comment()), e(req.author()), req.pause(), changes);
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		class ImageGetCallData : public asyncpp::grpc::calldata_interface {
		public:
			ImageGetCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_responder{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					spdlog::info("paused {} times", m_npaused);
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_responder.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else {
						if (!m_response.chunk().empty())
							m_responder.WriteAndFinish(m_response, ::grpc::WriteOptions{}, ::grpc::Status::OK, ptr_tag<tag::finish>(this));
						else
							m_responder.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
					}
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (!m_write_done) {
					m_npaused++;
					m_write_curl_paused = true;
					return CURL_WRITEFUNC_PAUSE;
				}
				if (m_response.chunk().size() < m_request.chunk_size()) {
					m_response.mutable_chunk()->append(data, size);
				} else {
					m_response.mutable_chunk()->append(data, size);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
					m_response.mutable_chunk()->clear();
				}
				return size;
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestImageGet(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ImageGetCallData(m_service, m_cq))->start();
					m_request.set_chunk_size(std::min<uint32_t>(std::max<uint32_t>(m_request.chunk_size(), 16 * 1024), 1024 * 1024));
					m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/images/{}/get", e(m_request.name())));
					m_hdl.set_option_long(CURLOPT_BUFFERSIZE, CURL_MAX_READ_SIZE);
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_done == false);
						m_write_done = true;
					}
					if (m_write_curl_paused) {
						m_service->get_client().get_executor().push([&]() {
							m_write_curl_paused = false;
							m_hdl.unpause(CURLPAUSE_RECV);
						});
					}
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ImageGetRequest m_request{};
			ImageGetResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncWriter<ImageGetResponse> m_responder;
			// Safety checking
			//bool m_finish_called{false};
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write };

			asyncpp::curl::handle m_hdl{};
			std::mutex m_write_mtx{};
			bool m_write_done{true};
			bool m_write_curl_paused{false};
			std::string m_error_buf{};
			std::string m_read_buf{};
			size_t m_npaused = 0;
		};

		class ImageGetAllCallData : public asyncpp::grpc::calldata_interface {
		public:
			ImageGetAllCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_responder{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					spdlog::info("paused {} times", m_npaused);
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_responder.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else {
						if (!m_response.chunk().empty())
							m_responder.WriteAndFinish(m_response, ::grpc::WriteOptions{}, ::grpc::Status::OK, ptr_tag<tag::finish>(this));
						else
							m_responder.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
					}
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (!m_write_done) {
					m_npaused++;
					m_write_curl_paused = true;
					return CURL_WRITEFUNC_PAUSE;
				}
				if (m_response.chunk().size() < m_request.chunk_size()) {
					m_response.mutable_chunk()->append(data, size);
				} else {
					m_response.mutable_chunk()->append(data, size);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
					m_response.mutable_chunk()->clear();
				}
				return size;
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestImageGetAll(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ImageGetAllCallData(m_service, m_cq))->start();
					m_request.set_chunk_size(std::min<uint32_t>(std::max<uint32_t>(m_request.chunk_size(), 16 * 1024), 1024 * 1024));
					std::string names = "";
					for (auto& n : m_request.names()) {
						if (!names.empty())
							names += "&names=";
						else
							names += "?names=";
						names += e(n);
					}
					m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/images/get{}", names));
					m_hdl.set_option_long(CURLOPT_BUFFERSIZE, CURL_MAX_READ_SIZE);
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_done == false);
						m_write_done = true;
					}
					if (m_write_curl_paused) {
						m_service->get_client().get_executor().push([&]() {
							m_write_curl_paused = false;
							m_hdl.unpause(CURLPAUSE_RECV);
						});
					}
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ImageGetAllRequest m_request{};
			ImageGetAllResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncWriter<ImageGetAllResponse> m_responder;
			// Safety checking
			//bool m_finish_called{false};
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write };

			asyncpp::curl::handle m_hdl{};
			std::mutex m_write_mtx{};
			bool m_write_done{true};
			bool m_write_curl_paused{false};
			std::string m_error_buf{};
			std::string m_read_buf{};
			size_t m_npaused = 0;
		};

		class ImageLoadCallData : public asyncpp::grpc::calldata_interface {
		public:
			ImageLoadCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_stream{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_stream.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else
						m_stream.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
				m_hdl.set_readfunction([this](char* ptr, size_t size) -> size_t {
					auto c = m_request.mutable_chunk();
					if (!c->empty()) {
						size = std::min(c->size(), size);
						memcpy(ptr, c->data(), size);
						c->erase(0, size);
						return size;
					} else {
						if (m_read_done) return 0;
						m_stream.Read(&m_request, ptr_tag<tag::read>(this));
						return CURL_READFUNC_PAUSE;
					}
				});
				m_hdl.set_option_bool(CURLOPT_POST, true);
				m_headers.append("Content-Type: application/x-tar");
				m_hdl.set_headers(m_headers);
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (!m_write_done) { return CURL_WRITEFUNC_PAUSE; }
				m_write_buf.append(data, size);
				auto pos = m_write_buf.find_first_of("\r\n");
				if (pos != std::string::npos) {
					process_line(std::string_view{m_write_buf.c_str(), pos});
					auto end = m_write_buf.find_first_not_of("\r\n", pos);
					m_write_buf.erase(0, end);
				}
				return size;
			}

			void process_line(std::string_view view) {
				try {
					auto j = nlohmann::json::parse(view);
					if (j.dump() == m_last_status) return;
					m_last_status = j.dump();
					m_response.Clear();
					json_map::from_json(j, m_response);
					m_write_done = false;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
				} catch (...) { spdlog::error("ImageLoadCallData({}) failed to parse json: \"{}\"", static_cast<void*>(this), view); }
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestImageLoad(&m_ctx, &m_stream, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ImageLoadCallData(m_service, m_cq))->start();
					m_stream.Read(&m_request, ptr_tag<tag::read>(this));
					break;
				}
				case tag::read: {
					if (m_running) {
						if (!ok) m_read_done = true;
						m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_SEND); });
					} else {
						m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/images/load?quiet=true"));
						m_service->get_client().get_executor().add_handle(m_hdl);
					}
					m_running = true;
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_done == false);
						m_write_done = true;
					}
					m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_RECV); });
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ImageLoadRequest m_request{};
			ImageLoadResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncReaderWriter<ImageLoadResponse, ImageLoadRequest> m_stream;
			// Safety checking
			//bool m_finish_called{false};
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write, read };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_headers;
			std::mutex m_write_mtx{};
			bool m_running{false};
			bool m_write_done{true};
			bool m_read_done{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};
		};

	} // namespace

	void DockerServiceImpl::start_async_calls_image(::grpc::ServerCompletionQueue* cq) {
		ImageListCallData(this, cq);
		(new ImageBuildCallData(this, cq))->start();
		ImageBuildPruneCallData(this, cq);
		ImageInspectCallData(this, cq);
		(new ImagePullCallData(this, cq))->start();
		ImageHistoryCallData(this, cq);
		(new ImagePushCallData(this, cq))->start();
		ImageTagCallData(this, cq);
		ImageDeleteCallData(this, cq);
		ImageSearchCallData(this, cq);
		ImagePruneCallData(this, cq);
		ImageCommitCallData(this, cq);
		(new ImageGetCallData(this, cq))->start();
		(new ImageGetAllCallData(this, cq))->start();
		(new ImageLoadCallData(this, cq))->start();
	}

} // namespace it::thalhammer::docker_gateway