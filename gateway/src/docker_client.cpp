#include <async_helpers.h>
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <cstdlib>
#include <curl/curl.h>
#include <docker_client.h>
#include <filesystem>
#include <spdlog/spdlog.h>

namespace it::thalhammer::docker_gateway {
	docker_client::docker_client() {}

	void docker_client::set_url(const std::string& url) { m_url = url; }

	void docker_client::prepare_handle(asyncpp::curl::handle& hdl, const std::string& url) const {
		if (m_url.starts_with("unix://")) {
			hdl.set_option_ptr(CURLOPT_UNIX_SOCKET_PATH, m_url.c_str() + 7);
			hdl.set_url("http://local" + url);
		} else if (m_url.starts_with("tcp://")) {
			hdl.set_url("http://" + m_url.substr(6) + url);
		} else {
			hdl.set_url(m_url + url);
		}
		//hdl.set_verbose(true);
	}

	nlohmann::json docker_client::check_return(asyncpp::curl::handle& hdl, const std::string& msg) {
		auto j = nlohmann::json::parse(msg, nullptr, false);
		if (hdl.get_response_code() / 100 > 3) {
			if (!j.is_discarded() && j.contains("message") && j["message"].is_string())
				throw map_http_error(hdl.get_response_code(), j["message"]);
			else
				throw map_http_error(hdl.get_response_code(), msg);
		}
		return j;
	}

	asyncpp::task<nlohmann::json> docker_client::get_json(const std::string url) {
		asyncpp::curl::handle hdl;
		prepare_handle(hdl, url);
		std::string res;
		hdl.set_writestring(res);
		auto code = co_await m_executor.exec(hdl);
		if (code != 0) throw std::runtime_error("curl request failed!");
		spdlog::debug("<= '{}'", res);
		co_return check_return(hdl, res);
	}

	asyncpp::task<std::multimap<std::string, std::string>> docker_client::head(const std::string url) {
		asyncpp::curl::handle hdl;
		prepare_handle(hdl, url);
		hdl.set_option_bool(CURLOPT_NOBODY, true);
		asyncpp::curl::slist headers;
		hdl.set_headerfunction_slist(headers);
		auto code = co_await m_executor.exec(hdl);
		if (code != 0) throw std::runtime_error("curl request failed!");
		if (hdl.get_response_code() / 100 > 3) { throw map_http_error(hdl.get_response_code(), ""); }
		std::multimap<std::string, std::string> res_headers;
		for (const auto e : headers) {
			std::string_view view{e};
			auto pos = view.find_first_of(':');
			if (pos == std::string::npos) continue;
			auto key = view.substr(0, pos);
			auto val = view.substr(pos + 1);
			asyncpp::curl::util::trim(key);
			asyncpp::curl::util::trim(val);
			res_headers.insert(std::make_pair(key, val));
		}
		co_return res_headers;
	}

	asyncpp::task<nlohmann::json> docker_client::delete_json(const std::string url) {
		asyncpp::curl::handle hdl;
		prepare_handle(hdl, url);
		std::string res;
		hdl.set_writestring(res);
		hdl.set_option_ptr(CURLOPT_CUSTOMREQUEST, "DELETE");
		auto code = co_await m_executor.exec(hdl);
		if (code != 0) throw std::runtime_error("curl request failed!");
		spdlog::debug("<= '{}'", res);
		co_return check_return(hdl, res);
	}

	asyncpp::task<nlohmann::json> docker_client::post_json(const std::string url, const nlohmann::json& body) {
		if (body.is_discarded()) co_return co_await post_json(url);

		asyncpp::curl::handle hdl;
		prepare_handle(hdl, url);
		std::string res;
		hdl.set_writestring(res);
		auto post_body = body.dump();
		spdlog::debug("=> '{}'", post_body);
		hdl.set_readstring(post_body);
		hdl.set_option_bool(CURLOPT_POST, true);
		hdl.set_option_long(CURLOPT_POSTFIELDSIZE, post_body.size());
		asyncpp::curl::slist headers;
		headers.append("Content-Type: application/json");
		hdl.set_headers(headers);
		auto code = co_await m_executor.exec(hdl);
		if (code != 0) throw std::runtime_error("curl request failed!");
		spdlog::debug("<= '{}'", res);
		co_return check_return(hdl, res);
	}

	asyncpp::task<nlohmann::json> docker_client::post_json(const std::string url) {
		asyncpp::curl::handle hdl;
		prepare_handle(hdl, url);
		std::string res;
		hdl.set_writestring(res);
		hdl.set_option_bool(CURLOPT_POST, true);
		hdl.set_option_long(CURLOPT_POSTFIELDSIZE, 0);
		auto code = co_await m_executor.exec(hdl);
		if (code != 0) throw std::runtime_error("curl request failed!");
		spdlog::debug("<= '{}'", res);
		co_return check_return(hdl, res);
	}

	asyncpp::task<void> docker_client::execute(docker_request& req) {
		asyncpp::curl::handle hdl;
		prepare_handle(hdl, req.url);
		hdl.set_writestring(req.response_body);
		hdl.set_readstring(req.request_body);
		hdl.set_headers(req.request_headers);
		hdl.set_headerfunction_slist(req.response_headers);
		switch (req.method) {
		case docker_request::method_type::GET: break;
		case docker_request::method_type::HEAD: hdl.set_option_bool(CURLOPT_NOBODY, true); break;
		case docker_request::method_type::DELETE: hdl.set_option_ptr(CURLOPT_CUSTOMREQUEST, "DELETE"); break;
		case docker_request::method_type::POST:
			hdl.set_option_bool(CURLOPT_POST, true);
			hdl.set_option_long(CURLOPT_POSTFIELDSIZE, req.request_body.size());
			break;
		}
		auto code = co_await m_executor.exec(hdl);
		if (code != 0) throw std::runtime_error("curl request failed!");
		spdlog::debug("<= '{}'", req.response_body);
		req.response_code = hdl.get_response_code();
	}

	std::string docker_client::find_docker_address() {
		if (getenv("DOCKER_HOST") != nullptr) { return getenv("DOCKER_HOST"); }
		if (std::filesystem::exists("/var/run/docker.sock")) { return "unix:///var/run/docker.sock"; }
		return "tcp://127.0.0.1:2375";
	}
} // namespace it::thalhammer::docker_gateway