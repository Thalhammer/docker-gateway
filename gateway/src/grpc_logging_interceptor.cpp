#include <grpc_logging_interceptor.h>
#include <grpcpp/impl/codegen/interceptor.h>
#include <spdlog/spdlog.h>

namespace {
	using ::grpc::experimental::InterceptionHookPoints;
	using ::grpc::experimental::InterceptorBatchMethods;
	using ::grpc::experimental::ServerRpcInfo;
	using hook_pair = std::pair<InterceptionHookPoints, const char*>;
	static constexpr hook_pair hook_map[] = {{InterceptionHookPoints::PRE_SEND_INITIAL_METADATA, "PRE_SEND_INITIAL_METADATA"},
											 {InterceptionHookPoints::PRE_SEND_MESSAGE, "PRE_SEND_MESSAGE"},
											 {InterceptionHookPoints::POST_SEND_MESSAGE, "POST_SEND_MESSAGE"},
											 {InterceptionHookPoints::PRE_SEND_STATUS, "PRE_SEND_STATUS"},
											 {InterceptionHookPoints::PRE_SEND_CLOSE, "PRE_SEND_CLOSE"},
											 {InterceptionHookPoints::PRE_RECV_INITIAL_METADATA, "PRE_RECV_INITIAL_METADATA"},
											 {InterceptionHookPoints::PRE_RECV_MESSAGE, "PRE_RECV_MESSAGE"},
											 {InterceptionHookPoints::PRE_RECV_STATUS, "PRE_RECV_STATUS"},
											 {InterceptionHookPoints::POST_RECV_INITIAL_METADATA, "POST_RECV_INITIAL_METADATA"},
											 {InterceptionHookPoints::POST_RECV_MESSAGE, "POST_RECV_MESSAGE"},
											 {InterceptionHookPoints::POST_RECV_STATUS, "POST_RECV_STATUS"},
											 {InterceptionHookPoints::POST_RECV_CLOSE, "POST_RECV_CLOSE"},
											 {InterceptionHookPoints::PRE_SEND_CANCEL, "PRE_SEND_CANCEL"}};

	class logging_interceptor : public ::grpc::experimental::Interceptor {
		ServerRpcInfo* m_info;
		std::shared_ptr<spdlog::logger> m_logger;
		std::chrono::steady_clock::time_point m_start_time = std::chrono::steady_clock::now();

	public:
		logging_interceptor(ServerRpcInfo* info, std::shared_ptr<spdlog::logger> logger) : m_info{info}, m_logger{logger} {}

		void Intercept(InterceptorBatchMethods* methods) override {
			if (m_logger->should_log(spdlog::level::debug)) {
				std::string hooks;
				for (auto e : hook_map) {
					if (methods->QueryInterceptionHookPoint(e.first)) {
						if (!hooks.empty()) hooks += ",";
						hooks += e.second;
						if (e.first == InterceptionHookPoints::PRE_SEND_STATUS) {
							auto status = methods->GetSendStatus();
							hooks += "{" + std::to_string(status.error_code()) + " " + status.error_message() + "}";
						}
					}
				}
				if (!hooks.empty()) m_logger->debug("{} {} ({})", static_cast<void*>(this), m_info->method(), hooks);
			}
			if (methods->QueryInterceptionHookPoint(InterceptionHookPoints::PRE_SEND_STATUS)) {
				auto status = methods->GetSendStatus();
				auto dur = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - m_start_time);
				m_logger->log(status.ok() ? spdlog::level::info : spdlog::level::warn, "{} finished with code {} after {} ms", m_info->method(),
							  status.error_code(), dur.count());
			}
			methods->Proceed();
		}
	};

	class logging_interceptor_factory : public ::grpc::experimental::ServerInterceptorFactoryInterface {
		std::shared_ptr<spdlog::logger> m_logger;

	public:
		logging_interceptor_factory(std::shared_ptr<spdlog::logger> logger) : m_logger{std::move(logger)} { assert(m_logger); }
		::grpc::experimental::Interceptor* CreateServerInterceptor(::grpc::experimental::ServerRpcInfo* info) override {
			return new logging_interceptor(info, m_logger);
		}
	};
} // namespace

namespace it::thalhammer::docker_gateway {
	std::unique_ptr<::grpc::experimental::ServerInterceptorFactoryInterface> create_grpc_logging_interceptor(std::shared_ptr<spdlog::logger> logger) {
		return std::make_unique<logging_interceptor_factory>(logger ? logger : spdlog::default_logger());
	}
} // namespace it::thalhammer::docker_gateway