#include "DockerServiceImpl.h"
#include <async_helpers.h>
#include <asyncpp/curl/exception.h>
#include <asyncpp/curl/executor.h>
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <curl/curl.h>
#include <json_map.h>
#include <regex>
#include <spdlog/spdlog.h>

using namespace asyncpp;

namespace it::thalhammer::docker_gateway {
	using namespace ::google::protobuf;

	namespace {

		CALL(ConfigListCallData, RequestConfigList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/configs?filters={}", e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ConfigCreateCallData, RequestConfigCreate, {
			auto res = co_await client.post_json("/v1.41/configs/create", json_map::to_json(req.spec()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ConfigInspectCallData, RequestConfigInspect, {
			auto res = co_await client.get_json("/v1.41/configs/{}", e(req.id()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ConfigDeleteCallData, RequestConfigDelete, {
			co_await client.delete_json("/v1.41/configs/{}", e(req.id()));
			co_return ::grpc::Status::OK;
		})

		CALL(ConfigUpdateCallData, RequestConfigUpdate, {
			co_await client.post_json("/v1.41/configs/{}/update?version={}", json_map::to_json(req.spec()), e(req.id()), req.version());
			co_return ::grpc::Status::OK;
		})

	} // namespace

	void DockerServiceImpl::start_async_calls_config(::grpc::ServerCompletionQueue* cq) {
		ConfigListCallData(this, cq);
		ConfigCreateCallData(this, cq);
		ConfigInspectCallData(this, cq);
		ConfigDeleteCallData(this, cq);
		ConfigUpdateCallData(this, cq);
	}
} // namespace it::thalhammer::docker_gateway