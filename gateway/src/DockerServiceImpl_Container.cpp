#include "DockerServiceImpl.h"
#include <async_helpers.h>
#include <asyncpp/curl/exception.h>
#include <asyncpp/curl/executor.h>
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <asyncpp/grpc/client_streaming_call.h>
#include <asyncpp/grpc/client_streaming_task.h>
#include <asyncpp/grpc/generic_method_awaiter.h>
#include <asyncpp/grpc/server_streaming_call.h>
#include <asyncpp/grpc/server_streaming_task.h>
#include <curl/curl.h>
#include <json_map.h>
#include <regex>
#include <spdlog/spdlog.h>
#include <tar_file.h>

using namespace asyncpp;

namespace it::thalhammer::docker_gateway {
	using namespace ::google::protobuf;

	namespace {

		CALL(ContainerListCallData, RequestContainerList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res =
				co_await client.get_json("/v1.41/containers/json?filters={}&all={}&limit={}&size={}", e(filters.dump()), req.all(), req.limit(), req.size());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerInspectCallData, RequestContainerInspect, {
			auto res = co_await client.get_json("/v1.41/containers/{}/json?size={}", e(req.id()), req.size());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerCreateCallData, RequestContainerCreate, {
			auto res = co_await client.post_json("/v1.41/containers/create?name={}", json_map::to_json(req.config()), e(req.name()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerTopCallData, RequestContainerTop, {
			auto res = co_await client.get_json("/v1.41/containers/{}/top?ps_args={}", e(req.id()), e(req.ps_args()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		class ContainerLogsCallData : public asyncpp::grpc::calldata_interface {
		public:
			ContainerLogsCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_stream{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_stream.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else
						m_stream.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (m_write_in_progress) { return CURL_WRITEFUNC_PAUSE; }
				if (m_is_tty) {
					m_response.mutable_frame()->set_stream_type(0);
					m_response.mutable_frame()->set_payload(data, size);
					m_write_in_progress = true;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
				} else {
					m_write_buf.append(data, size);
					using namespace asyncpp::curl::util;
					if (m_write_buf.size() >= 8) {
						auto stream = get<uint8_t>(m_write_buf.data());
						auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
						if (m_write_buf.size() >= len + 8) {
							m_response.mutable_frame()->set_stream_type(stream);
							m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
							m_write_buf.erase(0, len + 8);
							m_write_in_progress = true;
							m_stream.Write(m_response, ptr_tag<tag::write>(this));
						}
					}
				}
				return size;
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestContainerLogs(&m_ctx, &m_request, &m_stream, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ContainerLogsCallData(m_service, m_cq))->start();
					spdlog::info("Calling inspect on container {} to determine tty mode", m_request.id());
					m_inspect_request.set_id(m_request.id());
					m_stub = DockerService::NewStub(m_service->get_internal_channel());
					m_inspect_reader = m_stub->AsyncContainerInspect(&m_client_context, m_inspect_request, m_cq);
					m_inspect_reader->Finish(&m_inspect_response, &m_inspect_status, ptr_tag<tag::inspect_finish>(this));
					break;
				}
				case tag::inspect_finish: {
					if (!m_inspect_status.ok()) return m_stream.Finish(m_inspect_status, ptr_tag<tag::finish>(this));
					if (!m_inspect_response.has_info() || !m_inspect_response.info().has_config())
						return m_stream.Finish(::grpc::Status(::grpc::StatusCode::INTERNAL, "Missing inspect info"), ptr_tag<tag::finish>(this));
					m_is_tty = m_inspect_response.info().config().tty();
					spdlog::info("Attaching to {} in {} mode", m_inspect_response.info().id(), m_is_tty ? "tty" : "multiplexed");

					auto url = fmt::format("/v1.41/containers/{}/logs?follow={}&stdout={}&stderr={}&since={}&until={}&timestamps={}&tail={}",
										   e(m_inspect_response.info().id()), m_request.follow(), m_request.stdout_(), m_request.stderr_(), m_request.since(),
										   m_request.until(), m_request.timestamps(), m_request.tail() ? std::to_string(m_request.tail()) : "all");
					m_service->get_client().prepare_handle(m_hdl, url);
					m_service->get_client().get_executor().add_handle(m_hdl);
					std::lock_guard lck{m_write_mtx};
					m_response.mutable_info()->CopyFrom(m_inspect_response.info());
					m_write_in_progress = true;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_in_progress == true);
						m_response.clear_info();
						using namespace asyncpp::curl::util;
						if (m_write_buf.size() >= 8) {
							auto stream = get<uint8_t>(m_write_buf.data());
							auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
							if (m_write_buf.size() >= len + 8) {
								m_response.mutable_frame()->set_stream_type(stream);
								m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
								m_write_buf.erase(0, len + 8);
								m_stream.Write(m_response, ptr_tag<tag::write>(this));
								break;
							}
						}
						m_write_in_progress = false;
					}
					m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_RECV); });
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ContainerLogsRequest m_request{};
			ContainerLogsResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncWriter<ContainerLogsResponse> m_stream;
			// Safety checking
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write, read, inspect_finish };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_headers;
			std::mutex m_write_mtx{};
			bool m_write_in_progress{false};
			bool m_is_tty{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};

			// Inspect call data
			std::unique_ptr<DockerService::Stub> m_stub;
			::grpc::ClientContext m_client_context;
			ContainerInspectRequest m_inspect_request;
			ContainerInspectResponse m_inspect_response;
			::grpc::Status m_inspect_status;
			std::unique_ptr<::grpc::ClientAsyncResponseReader<ContainerInspectResponse>> m_inspect_reader;
		};

		CALL(ContainerChangesCallData, RequestContainerChanges, {
			auto res = co_await client.get_json("/v1.41/containers/{}/changes", e(req.id()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		class ContainerExportCallData : public asyncpp::grpc::calldata_interface {
		public:
			ContainerExportCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_responder{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					spdlog::debug("paused {} times", m_npaused);
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_responder.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else {
						if (!m_response.chunk().empty())
							m_responder.WriteAndFinish(m_response, ::grpc::WriteOptions{}, ::grpc::Status::OK, ptr_tag<tag::finish>(this));
						else
							m_responder.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
					}
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (!m_write_done) {
					m_npaused++;
					m_write_curl_paused = true;
					return CURL_WRITEFUNC_PAUSE;
				}
				if (m_response.chunk().size() < m_request.chunk_size()) {
					m_response.mutable_chunk()->append(data, size);
				} else {
					m_response.mutable_chunk()->append(data, size);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
					m_response.mutable_chunk()->clear();
				}
				return size;
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestContainerExport(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ContainerExportCallData(m_service, m_cq))->start();
					m_request.set_chunk_size(std::min<uint32_t>(std::max<uint32_t>(m_request.chunk_size(), 16 * 1024), 1024 * 1024));
					m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/containers/{}/export", e(m_request.id())));
					m_hdl.set_option_long(CURLOPT_BUFFERSIZE, CURL_MAX_READ_SIZE);
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_done == false);
						m_write_done = true;
					}
					if (m_write_curl_paused) {
						m_service->get_client().get_executor().push([&]() {
							m_write_curl_paused = false;
							m_hdl.unpause(CURLPAUSE_RECV);
						});
					}
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ContainerExportRequest m_request{};
			ContainerExportResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncWriter<ContainerExportResponse> m_responder;
			// Safety checking
			//bool m_finish_called{false};
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write };

			asyncpp::curl::handle m_hdl{};
			std::mutex m_write_mtx{};
			bool m_write_done{true};
			bool m_write_curl_paused{false};
			std::string m_error_buf{};
			std::string m_read_buf{};
			size_t m_npaused = 0;
		};

		class ContainerStatsCallData : public line_response_calldata<DockerServiceImpl, ContainerStatsRequest, ContainerStatsResponse> {
		public:
			ContainerStatsCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : line_response_calldata(service, cq) {}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestContainerStats(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

		private:
			void process_line(std::string_view view) override {
				try {
					auto j = nlohmann::json::parse(view);
					m_response.Clear();
					json_map::from_json(j, m_response);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
				} catch (...) { spdlog::error("ContainerStatsCallData({}) failed to parse json: \"{}\"", static_cast<void*>(this), view); }
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ContainerStatsCallData(m_service, m_cq))->start();
					m_service->get_client().prepare_handle(
						m_hdl, fmt::format("/v1.41/containers/{}/stats?stream={}&one-shot={}", e(m_request.id()), m_request.stream(), m_request.one_shot()));
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				default: return line_response_calldata::handle_event(evt, ok);
				}
			}
		};

		CALL(ContainerResizeCallData, RequestContainerResize, {
			co_await client.post_json("/v1.41/containers/{}/resize?w={}&h={}", no_body, e(req.id()), req.width(), req.height());
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerStartCallData, RequestContainerStart, {
			co_await client.post_json("/v1.41/containers/{}/start?detachKeys={}", no_body, e(req.id()), e(req.detach_keys()));
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerStopCallData, RequestContainerStop, {
			co_await client.post_json("/v1.41/containers/{}/stop?t={}", no_body, e(req.id()), req.timeout());
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerRestartCallData, RequestContainerRestart, {
			co_await client.post_json("/v1.41/containers/{}/restart?t={}", no_body, e(req.id()), req.timeout());
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerKillCallData, RequestContainerKill, {
			co_await client.post_json("/v1.41/containers/{}/kill?signal={}", no_body, e(req.id()), e(req.signal()));
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerUpdateCallData, RequestContainerUpdate, {
			auto res = co_await client.post_json("/v1.41/containers/{}/update", json_map::to_json(req.update()), e(req.id()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerRenameCallData, RequestContainerRename, {
			co_await client.post_json("/v1.41/containers/{}/rename?name={}", no_body, e(req.id()), e(req.name()));
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerPauseCallData, RequestContainerPause, {
			co_await client.post_json("/v1.41/containers/{}/pause", no_body, e(req.id()));
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerUnpauseCallData, RequestContainerUnpause, {
			co_await client.post_json("/v1.41/containers/{}/unpause", no_body, e(req.id()));
			co_return ::grpc::Status::OK;
		})

		class ContainerAttachCallData : public asyncpp::grpc::calldata_interface {
		public:
			ContainerAttachCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_stream{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_stream.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else
						m_stream.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
				m_hdl.set_readfunction([this](char* ptr, size_t size) -> size_t {
					if (m_read_in_progress) return CURL_READFUNC_PAUSE;
					auto c = m_request.mutable_frame();
					if (!c->payload().empty()) {
						if (m_is_tty) {
							size = std::min(c->payload().size(), size);
							memcpy(ptr, c->payload().data(), size);
							c->mutable_payload()->erase(0, size);
							return size;
						} else {
							size = std::min(c->payload().size() + 8, size);
							using namespace asyncpp::curl::util;
							size -= 8;
							memset(ptr, 0, 8);
							set<uint8_t>(ptr, c->stream_type());
							set<uint32_t>(ptr + 4, native_to_big<uint32_t>(size));
							ptr += 8;
							memcpy(ptr, c->payload().data(), size);
							c->mutable_payload()->erase(0, size);
							return size + 8;
						}
					} else {
						if (m_read_done) return 0;
						assert(m_read_in_progress == false);
						m_read_in_progress = true;
						m_stream.Read(&m_request, ptr_tag<tag::read>(this));
						return CURL_READFUNC_PAUSE;
					}
				});
				m_hdl.set_option_bool(CURLOPT_POST, true);
				m_headers.append("Transfer-Encoding:");
				m_headers.append("Connection: Upgrade");
				m_headers.append("Upgrade: tcp");
				m_hdl.set_headers(m_headers);
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (m_write_in_progress) { return CURL_WRITEFUNC_PAUSE; }
				if (m_is_tty) {
					m_response.mutable_frame()->set_stream_type(0);
					m_response.mutable_frame()->set_payload(data, size);
					m_write_in_progress = true;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
				} else {
					m_write_buf.append(data, size);
					using namespace asyncpp::curl::util;
					if (m_write_buf.size() >= 8) {
						auto stream = get<uint8_t>(m_write_buf.data());
						auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
						if (m_write_buf.size() >= len + 8) {
							m_response.mutable_frame()->set_stream_type(stream);
							m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
							m_write_buf.erase(0, len + 8);
							m_write_in_progress = true;
							m_stream.Write(m_response, ptr_tag<tag::write>(this));
						}
					}
				}
				return size;
			}

			void process_frame(int64_t streamid, std::string_view view) {}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestContainerAttach(&m_ctx, &m_stream, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ContainerAttachCallData(m_service, m_cq))->start();
					m_read_in_progress = true;
					m_stream.Read(&m_request, ptr_tag<tag::read>(this));
					break;
				}
				case tag::read: {
					m_read_in_progress = false;
					if (m_running) {
						if (!ok) m_read_done = true;
						m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_SEND); });
					} else {
						if (!m_request.has_metadata()) {
							return m_stream.Finish(::grpc::Status(::grpc::StatusCode::INVALID_ARGUMENT, "Metadata is missing"), ptr_tag<tag::finish>(this));
						}
						auto& meta = m_request.metadata();
						spdlog::info("Calling inspect on container {} to determine tty mode", meta.id());
						m_inspect_request.set_id(meta.id());
						m_stub = DockerService::NewStub(m_service->get_internal_channel());
						m_inspect_reader = m_stub->AsyncContainerInspect(&m_client_context, m_inspect_request, m_cq);
						m_inspect_reader->Finish(&m_inspect_response, &m_inspect_status, ptr_tag<tag::inspect_finish>(this));
					}
					break;
				}
				case tag::inspect_finish: {
					auto& meta = m_request.metadata();
					if (!m_inspect_status.ok()) return m_stream.Finish(m_inspect_status, ptr_tag<tag::finish>(this));
					if (!m_inspect_response.has_info() || !m_inspect_response.info().has_config())
						return m_stream.Finish(::grpc::Status(::grpc::StatusCode::INTERNAL, "Missing inspect info"), ptr_tag<tag::finish>(this));
					m_is_tty = m_inspect_response.info().config().tty();
					spdlog::info("Attaching to {} in {} mode", m_inspect_response.info().id(), m_is_tty ? "tty" : "multiplexed");

					auto url = fmt::format("/v1.41/containers/{}/attach?detachKeys={}&logs={}&stream={}&stdin={}&stdout={}&stderr={}",
										   e(m_inspect_response.info().id()), e(meta.detach_keys()), meta.logs(), meta.stream(), meta.stdin_(), meta.stdout_(),
										   meta.stderr_());
					m_service->get_client().prepare_handle(m_hdl, url);
					m_hdl.set_option_bool(CURLOPT_UPLOAD, true);
					m_hdl.set_option_ptr(CURLOPT_CUSTOMREQUEST, "POST");
					m_hdl.set_option_bool(CURLOPT_FORBID_REUSE, true);
					m_hdl.set_option_bool(CURLOPT_FRESH_CONNECT, true);
					m_hdl.set_option_bool(CURLOPT_SUPPRESS_CONNECT_HEADERS, true);
					m_hdl.set_option_long(CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
					m_hdl.set_option_long(CURLOPT_REDIR_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
					//m_hdl.set_option_long(CURLOPT_TIMEOUT_MS, 5000);
					m_service->get_client().get_executor().add_handle(m_hdl);
					m_running = true;
					std::lock_guard lck{m_write_mtx};
					m_response.mutable_info()->CopyFrom(m_inspect_response.info());
					m_write_in_progress = true;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_in_progress == true);
						m_response.clear_info();
						using namespace asyncpp::curl::util;
						if (m_write_buf.size() >= 8) {
							auto stream = get<uint8_t>(m_write_buf.data());
							auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
							if (m_write_buf.size() >= len + 8) {
								m_response.mutable_frame()->set_stream_type(stream);
								m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
								m_write_buf.erase(0, len + 8);
								m_stream.Write(m_response, ptr_tag<tag::write>(this));
								break;
							}
						}
						m_write_in_progress = false;
					}
					m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_RECV); });
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ContainerAttachRequest m_request{};
			ContainerAttachResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncReaderWriter<ContainerAttachResponse, ContainerAttachRequest> m_stream;
			// Safety checking
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write, read, inspect_finish };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_headers;
			std::mutex m_write_mtx{};
			bool m_running{false};
			bool m_write_in_progress{false};
			bool m_read_in_progress{false};
			bool m_read_done{false};
			bool m_is_tty{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};

			// Inspect call data
			std::unique_ptr<DockerService::Stub> m_stub;
			::grpc::ClientContext m_client_context;
			ContainerInspectRequest m_inspect_request;
			ContainerInspectResponse m_inspect_response;
			::grpc::Status m_inspect_status;
			std::unique_ptr<::grpc::ClientAsyncResponseReader<ContainerInspectResponse>> m_inspect_reader;
		};

		CALL(ContainerWaitCallData, RequestContainerWait, {
			auto res = co_await client.post_json("/v1.41/containers/{}/wait?condition={}", no_body, e(req.id()), e(req.condition()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerDeleteCallData, RequestContainerDelete, {
			co_await client.delete_json("/v1.41/containers/{}?v={}&force={}&link={}", e(req.id()), req.remove_anonymous_volumes(), req.force(), req.link());
			co_return ::grpc::Status::OK;
		})

		CALL(ContainerArchiveInfoCallData, RequestContainerArchiveInfo, {
			auto res = co_await client.head("/v1.41/containers/{}/archive?path={}", e(req.id()), e(req.path()));
			auto it = res.find("X-Docker-Container-Path-Stat");
			if (it == res.end()) throw std::runtime_error("Missing stat header");
			auto j = nlohmann::json::parse(asyncpp::curl::util::base64_decode(it->second), nullptr, false);
			if (j.is_discarded()) throw std::runtime_error("invalid json in stat header");
			json_map::from_json(j, *resp.mutable_info());
			co_return ::grpc::Status::OK;
		})

		class ContainerArchiveCallData : public asyncpp::grpc::calldata_interface {
		public:
			ContainerArchiveCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_responder{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					spdlog::debug("paused {} times", m_npaused);
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_responder.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else {
						if (!m_response.chunk().empty())
							m_responder.WriteAndFinish(m_response, ::grpc::WriteOptions{}, ::grpc::Status::OK, ptr_tag<tag::finish>(this));
						else
							m_responder.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
					}
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (!m_write_done) {
					m_npaused++;
					m_write_curl_paused = true;
					return CURL_WRITEFUNC_PAUSE;
				}
				if (m_response.chunk().size() < m_request.chunk_size()) {
					m_response.mutable_chunk()->append(data, size);
				} else {
					m_response.mutable_chunk()->append(data, size);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
					m_response.mutable_chunk()->clear();
				}
				return size;
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestContainerArchive(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ContainerArchiveCallData(m_service, m_cq))->start();
					m_request.set_chunk_size(std::min<uint32_t>(std::max<uint32_t>(m_request.chunk_size(), 16 * 1024), 1024 * 1024));
					m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/containers/{}/archive?path={}", e(m_request.id()), e(m_request.path())));
					m_hdl.set_option_long(CURLOPT_BUFFERSIZE, CURL_MAX_READ_SIZE);
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_done == false);
						m_write_done = true;
					}
					if (m_write_curl_paused) {
						m_service->get_client().get_executor().push([&]() {
							m_write_curl_paused = false;
							m_hdl.unpause(CURLPAUSE_RECV);
						});
					}
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ContainerArchiveRequest m_request{};
			ContainerArchiveResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncWriter<ContainerArchiveResponse> m_responder;
			// Safety checking
			//bool m_finish_called{false};
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write };

			asyncpp::curl::handle m_hdl{};
			std::mutex m_write_mtx{};
			bool m_write_done{true};
			bool m_write_curl_paused{false};
			std::string m_error_buf{};
			std::string m_read_buf{};
			size_t m_npaused = 0;
		};

		class ContainerArchivePutCallData : public asyncpp::grpc::calldata_interface {
		public:
			ContainerArchivePutCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_stream{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_stream.FinishWithError(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else {
						m_stream.Finish(m_response, ::grpc::Status::OK, ptr_tag<tag::finish>(this));
					}
				});
				m_hdl.set_readfunction([this](char* ptr, size_t size) -> size_t {
					auto c = m_request.mutable_chunk();
					if (!c->empty()) {
						size = std::min(c->size(), size);
						memcpy(ptr, c->data(), size);
						c->erase(0, size);
						return size;
					} else {
						if (m_read_done) return 0;
						m_stream.Read(&m_request, ptr_tag<tag::read>(this));
						return CURL_READFUNC_PAUSE;
					}
				});
				m_hdl.set_option_bool(CURLOPT_UPLOAD, true);
				m_headers.append("Content-Type: application/x-tar");
				m_hdl.set_headers(m_headers);
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				m_write_buf.append(data, size);
				return size;
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestContainerArchivePut(&m_ctx, &m_stream, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ContainerArchivePutCallData(m_service, m_cq))->start();
					m_stream.Read(&m_request, ptr_tag<tag::read>(this));
					break;
				}
				case tag::read: {
					if (m_running) {
						if (!ok) m_read_done = true;
						m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_SEND); });
					} else {
						m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/containers/{}/archive?path={}&noOverwriteDirNonDir={}&copyUIDGID={}",
																				  e(m_request.id()), e(m_request.path()), m_request.no_overwrite_dir_non_dir(),
																				  m_request.copy_uidgid()));
						m_service->get_client().get_executor().add_handle(m_hdl);
					}
					m_running = true;
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_is_cancelled = true;
						m_read_done = true;
						m_service->get_client().get_executor().remove_handle(m_hdl);
						m_stream.FinishWithError(::grpc::Status::CANCELLED, ptr_tag<tag::finish>(this));
					}
					break;
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ContainerArchivePutRequest m_request{};
			ContainerArchivePutResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncReader<ContainerArchivePutResponse, ContainerArchivePutRequest> m_stream;
			// Safety checking
			//bool m_finish_called{false};
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, read };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_headers;
			std::mutex m_write_mtx{};
			bool m_running{false};
			bool m_read_done{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};
		};

		CALL(ContainerPruneCallData, RequestContainerPrune, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.post_json("/v1.41/containers/prune?filters={}", no_body, e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		asyncpp::grpc::client_streaming_task<ContainerFilePutRequest, ContainerFilePutResponse> ContainerFilePutCallData(DockerServiceImpl* service,
																														 ::grpc::ServerCompletionQueue* cq) {
			auto [ok, resp, ctx] = co_await asyncpp::grpc::start(&DockerService::AsyncService::RequestContainerFilePut, service, cq);
			if (!ok) co_return ::grpc::Status::CANCELLED;
			ContainerFilePutCallData(service, cq);

			// Read initial request
			ContainerFilePutRequest init_request;
			ok = co_await asyncpp::grpc::read(init_request);
			if (!ok) co_return ::grpc::Status::OK;

			if (!init_request.has_info()) { co_return ::grpc::Status(::grpc::StatusCode::INVALID_ARGUMENT, "Missing file info"); }
			if (!init_request.has_request()) { co_return ::grpc::Status(::grpc::StatusCode::INVALID_ARGUMENT, "Missing request info"); }

			auto stub = DockerService::NewStub(service->get_internal_channel());
			asyncpp::grpc::client_streaming_call<&DockerService::StubInterface::AsyncContainerArchivePut> call{ctx};
			for (auto& e : ctx.client_metadata())
				call.context()->AddMetadata(std::string{e.first.data(), e.first.size()}, std::string{e.second.data(), e.second.size()});

			ok = co_await call.start(stub.get(), cq);
			if (!ok) co_return ::grpc::Status(::grpc::StatusCode::INTERNAL, "Failed to initiate internal rpc");

			size_t write_size = 0;
			ContainerFilePutRequest request = init_request;
			do {
				ContainerArchivePutRequest msg{};
				msg.set_id(init_request.request().id());
				msg.set_path(init_request.request().path());
				msg.set_no_overwrite_dir_non_dir(init_request.request().no_overwrite_dir_non_dir());
				msg.set_copy_uidgid(init_request.request().copy_uidgid());

				if (request.has_info()) { // A new file was started, emit tar header
					// Previous chunk did not end on a nice border
					if (write_size % 512 != 0) { msg.mutable_chunk()->resize(512 - (write_size % 512)); }
					tar::header header = {};
					header.set_filemode(request.info().mode());
					header.set_uid(request.info().uid());
					header.set_gid(request.info().gid());
					header.set_filesize(request.info().size());
					header.set_last_modified(request.info().last_modified());
					header.set_type(static_cast<int>(request.info().type()));
					header.set_linked_file(request.info().linked_file());
					header.set_user_name(request.info().uid_name());
					header.set_group_name(request.info().uid_name());
					header.set_device_major(request.info().device_major());
					header.set_device_minor(request.info().device_minor());
					header.set_filename(request.info().name());
					header.finalize();

					msg.mutable_chunk()->append(reinterpret_cast<const char*>(&header), sizeof(header));
				}

				// Append payload data
				msg.mutable_chunk()->append(request.chunk());

				// Write output chunk
				ok = co_await call.write(msg);
				write_size += msg.chunk().size();
				request.Clear();
				if (!ok) break;

				ok = co_await asyncpp::grpc::read(request);
			} while (ok);
			{
				// Write final block
				ContainerArchivePutRequest msg{};
				msg.set_id(init_request.request().id());
				msg.set_path(init_request.request().path());
				msg.set_no_overwrite_dir_non_dir(init_request.request().no_overwrite_dir_non_dir());
				msg.set_copy_uidgid(init_request.request().copy_uidgid());
				msg.mutable_chunk()->resize(0);
				if (write_size % 512 != 0) { msg.mutable_chunk()->resize(512 - (write_size % 512)); }
				msg.mutable_chunk()->resize(msg.chunk().size() + 1024, 0);
				ok = co_await call.write(msg);
				write_size += msg.chunk().size();
			}
			auto result = co_await call.finish();
			for (auto& e : call.context()->GetServerTrailingMetadata()) {
				ctx.AddTrailingMetadata(std::string{e.first.data(), e.first.size()}, std::string{e.second.data(), e.second.size()});
			}
			co_return result;
		}

		asyncpp::grpc::server_streaming_task<ContainerFileGetResponse> ContainerFileGetCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) {
			auto [ok, req, ctx] = co_await asyncpp::grpc::start(&DockerService::AsyncService::RequestContainerFileGet, service, cq);
			if (!ok) co_return ::grpc::Status::CANCELLED;
			ContainerFileGetCallData(service, cq);

			auto stub = DockerService::NewStub(service->get_internal_channel());
			asyncpp::grpc::server_streaming_call<&DockerService::StubInterface::AsyncContainerArchive> call{ctx};
			for (auto& e : ctx.client_metadata())
				call.context()->AddMetadata(std::string{e.first.data(), e.first.size()}, std::string{e.second.data(), e.second.size()});

			ContainerArchiveRequest archive_req{};
			archive_req.set_id(req.id());
			archive_req.set_path(req.path());
			archive_req.set_chunk_size(req.chunk_size());
			ok = co_await call.start(stub.get(), archive_req, cq);
			if (!ok) co_return ::grpc::Status(::grpc::StatusCode::INTERNAL, "Failed to initiate internal rpc");

			ContainerArchiveResponse archive_resp;
			tar::parser parser{};
			ContainerFileGetResponse resp;
			parser.on_header = [&](const tar::header& hdr) {
				if (resp.has_info() || !resp.chunk().empty()) {
					// Interrupt parser, cause we need to send last packet first
					return false;
				}
				auto info = resp.mutable_info();
				info->set_name(hdr.get_filename());
				info->set_mode(hdr.get_filemode());
				info->set_uid(hdr.get_uid());
				info->set_gid(hdr.get_gid());
				info->set_size(hdr.get_filesize());
				info->set_last_modified(hdr.get_last_modified());
				info->set_type(static_cast<FileInfo_Type>(hdr.get_type()));
				info->set_linked_file(hdr.get_linked_file());
				info->set_uid_name(hdr.get_user_name());
				info->set_gid_name(hdr.get_group_name());
				info->set_device_major(hdr.get_device_major());
				info->set_device_minor(hdr.get_device_minor());
				return true;
			};
			parser.on_data = [&, chunksize = req.chunk_size()](std::string_view data) {
				if (resp.chunk().size() + data.size() > chunksize && !resp.chunk().empty()) return false;
				resp.mutable_chunk()->append(data);
				return true;
			};
			while (co_await call.read(archive_resp)) {
				std::string_view data = archive_resp.chunk();
				while (!data.empty()) {
					auto parsed = parser.parse(data);
					data.remove_prefix(parsed);
					if (resp.has_info() || !resp.chunk().empty()) {
						co_await asyncpp::grpc::write(resp);
						resp.clear_chunk();
						resp.clear_info();
					}
				}
			}

			co_return co_await call.finish();
		}

	} // namespace

	void DockerServiceImpl::start_async_calls_container(::grpc::ServerCompletionQueue* cq) {
		ContainerListCallData(this, cq);
		ContainerInspectCallData(this, cq);
		ContainerCreateCallData(this, cq);
		ContainerTopCallData(this, cq);
		(new ContainerLogsCallData(this, cq))->start();
		ContainerChangesCallData(this, cq);
		(new ContainerExportCallData(this, cq))->start();
		(new ContainerStatsCallData(this, cq))->start();
		ContainerResizeCallData(this, cq);
		ContainerStartCallData(this, cq);
		ContainerStopCallData(this, cq);
		ContainerRestartCallData(this, cq);
		ContainerKillCallData(this, cq);
		ContainerUpdateCallData(this, cq);
		ContainerRenameCallData(this, cq);
		ContainerPauseCallData(this, cq);
		ContainerUnpauseCallData(this, cq);
		(new ContainerAttachCallData(this, cq))->start();
		ContainerWaitCallData(this, cq);
		ContainerDeleteCallData(this, cq);
		ContainerArchiveInfoCallData(this, cq);
		(new ContainerArchiveCallData(this, cq))->start();
		(new ContainerArchivePutCallData(this, cq))->start();
		ContainerPruneCallData(this, cq);
		ContainerFilePutCallData(this, cq);
		ContainerFileGetCallData(this, cq);
	}
} // namespace it::thalhammer::docker_gateway