#include <DockerServiceImpl.h>
#include <fstream>
#include <grpc_logging_interceptor.h>
#include <grpc_thread_pool.h>
#include <spdlog/cfg/env.h>
#include <spdlog/spdlog.h>
#include <sstream>

using namespace it::thalhammer::docker_gateway;
using namespace asyncpp;
using namespace asyncpp::grpc;

std::string getenv_string(const char* name, const char* dflt = "") {
	const char* v = getenv(name);
	if (!v) v = dflt;
	if (!v) throw std::runtime_error("Missing value for " + std::string(name));
	return v;
}

bool getenv_bool(const char* name, bool dflt) {
	auto str = getenv_string(name, dflt ? "true" : "false");
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	return str == "true" || str == "1" || str == "yes" || str == "y" || str == "on";
}

std::string getenv_file(const char* name, const char* dflt) {
	const char* v = getenv(name);
	if (!v) v = dflt;
	if (!v) throw std::runtime_error("Missing value for " + std::string(name));
	std::ifstream file{v, std::ios::binary};
	if (!file) throw std::runtime_error("Failed open file " + std::string(v) + " for " + std::string(name));
	std::ostringstream ss;
	ss << file.rdbuf();
	return ss.str();
}

std::shared_ptr<::grpc::ServerCredentials> build_server_credentials() {
	if (getenv_bool("GRPC_TLS_ENABLED", false)) {
		::grpc::SslServerCredentialsOptions options;
		options.pem_root_certs = "";
		auto& pair = options.pem_key_cert_pairs.emplace_back();
		pair.private_key = getenv_file("GRPC_TLS_KEY", nullptr);
		pair.cert_chain = getenv_file("GRPC_TLS_CERT", nullptr);
		return ::grpc::SslServerCredentials(std::move(options));
	} else {
		return ::grpc::InsecureServerCredentials();
	}
}

int main(int argc, const char** argv) {
	auto def = spdlog::default_logger();
	auto grpc_call_logger = std::make_shared<spdlog::logger>("grpc_call", def->sinks().begin(), def->sinks().end());
	spdlog::register_logger(grpc_call_logger);
	spdlog::cfg::load_env_levels();
	constexpr const char* const listen_addr = "0.0.0.0:50051";
	DockerServiceImpl service{};
	::grpc::ServerBuilder builder{};
	builder.AddListeningPort(listen_addr, build_server_credentials());
	builder.RegisterService(&service);
	std::vector<std::unique_ptr<::grpc::experimental::ServerInterceptorFactoryInterface>> interceptors;
	interceptors.push_back(create_grpc_logging_interceptor(grpc_call_logger));
	builder.experimental().SetInterceptorCreators(std::move(interceptors));
	grpc_thread_pool pool{};
	auto server = pool.start(builder, 1);
	service.set_internal_channel(server->InProcessChannel(::grpc::ChannelArguments{}));
	pool.apply_cq([&service](::grpc::ServerCompletionQueue* cq) { service.start_async_calls(cq); });
	spdlog::info("Server listening on {}", listen_addr);
	std::cin.get();
	spdlog::info("Server stopping");
	pool.stop();
	spdlog::info("Server stopped");
}
