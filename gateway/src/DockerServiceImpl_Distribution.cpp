#include "DockerServiceImpl.h"
#include <async_helpers.h>
#include <asyncpp/curl/exception.h>
#include <asyncpp/curl/executor.h>
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <curl/curl.h>
#include <json_map.h>
#include <regex>
#include <spdlog/spdlog.h>

using namespace asyncpp;

namespace it::thalhammer::docker_gateway {
	using namespace ::google::protobuf;

	namespace {

		CALL(DistributionInspectCallData, RequestDistributionInspect, {
			auto res = co_await client.get_json("/v1.41/distribution/{}/json", req.name());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

	} // namespace

	void DockerServiceImpl::start_async_calls_distribution(::grpc::ServerCompletionQueue* cq) { DistributionInspectCallData(this, cq); }
} // namespace it::thalhammer::docker_gateway