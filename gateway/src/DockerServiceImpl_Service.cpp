#include "DockerServiceImpl.h"
#include <async_helpers.h>
#include <asyncpp/curl/exception.h>
#include <asyncpp/curl/executor.h>
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <curl/curl.h>
#include <json_map.h>
#include <regex>
#include <spdlog/spdlog.h>

using namespace asyncpp;

namespace it::thalhammer::docker_gateway {
	using namespace ::google::protobuf;

	namespace {

		CALL(TaskListCallData, RequestTaskList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/tasks?filters={}", e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(TaskInspectCallData, RequestTaskInspect, {
			auto res = co_await client.get_json("/v1.41/tasks/{}", e(req.id()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		class TaskLogsCallData : public asyncpp::grpc::calldata_interface {
		public:
			TaskLogsCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_stream{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_stream.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else
						m_stream.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (m_write_in_progress) { return CURL_WRITEFUNC_PAUSE; }
				if (m_is_tty) {
					m_response.mutable_frame()->set_stream_type(0);
					m_response.mutable_frame()->set_payload(data, size);
					m_write_in_progress = true;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
				} else {
					m_write_buf.append(data, size);
					using namespace asyncpp::curl::util;
					if (m_write_buf.size() >= 8) {
						auto stream = get<uint8_t>(m_write_buf.data());
						auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
						if (m_write_buf.size() >= len + 8) {
							m_response.mutable_frame()->set_stream_type(stream);
							m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
							m_write_buf.erase(0, len + 8);
							m_write_in_progress = true;
							m_stream.Write(m_response, ptr_tag<tag::write>(this));
						}
					}
				}
				return size;
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestTaskLogs(&m_ctx, &m_request, &m_stream, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new TaskLogsCallData(m_service, m_cq))->start();
					spdlog::info("Calling inspect on task {} to determine tty mode", m_request.id());
					m_inspect_request.set_id(m_request.id());
					m_stub = DockerService::NewStub(m_service->get_internal_channel());
					m_inspect_reader = m_stub->AsyncTaskInspect(&m_client_context, m_inspect_request, m_cq);
					m_inspect_reader->Finish(&m_inspect_response, &m_inspect_status, ptr_tag<tag::inspect_finish>(this));
					break;
				}
				case tag::inspect_finish: {
					if (!m_inspect_status.ok()) return m_stream.Finish(m_inspect_status, ptr_tag<tag::finish>(this));
					if (!m_inspect_response.has_info() || !m_inspect_response.info().has_spec() || !m_inspect_response.info().spec().has_container_spec())
						return m_stream.Finish(::grpc::Status(::grpc::StatusCode::INTERNAL, "Missing inspect info"), ptr_tag<tag::finish>(this));
					m_is_tty = m_inspect_response.info().spec().container_spec().tty();
					spdlog::info("Attaching to {} in {} mode", m_inspect_response.info().id(), m_is_tty ? "tty" : "multiplexed");

					auto url = fmt::format("/v1.41/tasks/{}/logs?details={}&follow={}&stdout={}&stderr={}&since={}&timestamps={}&tail={}",
										   e(m_inspect_response.info().id()), m_request.details(), m_request.follow(), m_request.stdout_(), m_request.stderr_(),
										   m_request.since(), m_request.timestamps(), m_request.tail() ? std::to_string(m_request.tail()) : "all");
					m_service->get_client().prepare_handle(m_hdl, url);
					m_service->get_client().get_executor().add_handle(m_hdl);
					std::lock_guard lck{m_write_mtx};
					m_response.mutable_info()->CopyFrom(m_inspect_response.info());
					m_write_in_progress = true;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_in_progress == true);
						m_response.clear_info();
						using namespace asyncpp::curl::util;
						if (m_write_buf.size() >= 8) {
							auto stream = get<uint8_t>(m_write_buf.data());
							auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
							if (m_write_buf.size() >= len + 8) {
								m_response.mutable_frame()->set_stream_type(stream);
								m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
								m_write_buf.erase(0, len + 8);
								m_stream.Write(m_response, ptr_tag<tag::write>(this));
								break;
							}
						}
						m_write_in_progress = false;
					}
					m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_RECV); });
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			TaskLogsRequest m_request{};
			TaskLogsResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncWriter<TaskLogsResponse> m_stream;
			// Safety checking
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write, read, inspect_finish };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_headers;
			std::mutex m_write_mtx{};
			bool m_write_in_progress{false};
			bool m_is_tty{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};

			// Inspect call data
			std::unique_ptr<DockerService::Stub> m_stub;
			::grpc::ClientContext m_client_context;
			TaskInspectRequest m_inspect_request;
			TaskInspectResponse m_inspect_response;
			::grpc::Status m_inspect_status;
			std::unique_ptr<::grpc::ClientAsyncResponseReader<TaskInspectResponse>> m_inspect_reader;
		};

		CALL(ServiceListCallData, RequestServiceList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/services?filters={}&status={}", e(filters.dump()), req.status());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ServiceCreateCallData, RequestServiceCreate, {
			auto http_req = docker_request::post_json(json_map::to_json(req.spec()), "/v1.41/services/create");
			if (req.has_registry_auth()) {
				http_req.request_headers.append(("X-Registry-Auth: " + DockerServiceImpl::build_registry_auth(req.registry_auth())).c_str());
			}
			co_await client.execute(http_req);
			auto j = nlohmann::json::parse(http_req.response_body, nullptr, false);
			if (http_req.response_code / 100 > 3) {
				if (!j.is_discarded() && j.contains("message") && j["message"].is_string())
					throw map_http_error(http_req.response_code, j["message"]);
				else
					throw map_http_error(http_req.response_code, http_req.response_body);
			}
			json_map::from_json(j, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ServiceInspectCallData, RequestServiceInspect, {
			auto res = co_await client.get_json("/v1.41/services/{}?insertDefaults={}", e(req.id()), req.insert_defaults());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(ServiceDeleteCallData, RequestServiceDelete, {
			co_await client.delete_json("/v1.41/services/{}", e(req.id()));
			co_return ::grpc::Status::OK;
		})

		CALL(ServiceUpdateCallData, RequestServiceUpdate, {
			auto http_req = docker_request::post_json(json_map::to_json(req.spec()), "/v1.41/services/{}/update?version={}&registryAuthFrom={}&rollback={}",
													  e(req.id()), req.version(), e(req.registry_auth_from()), req.rollback() ? "previous" : "");
			if (req.has_registry_auth()) {
				http_req.request_headers.append(("X-Registry-Auth: " + DockerServiceImpl::build_registry_auth(req.registry_auth())).c_str());
			}
			co_await client.execute(http_req);
			auto j = nlohmann::json::parse(http_req.response_body, nullptr, false);
			if (http_req.response_code / 100 > 3) {
				if (!j.is_discarded() && j.contains("message") && j["message"].is_string())
					throw map_http_error(http_req.response_code, j["message"]);
				else
					throw map_http_error(http_req.response_code, http_req.response_body);
			}
			json_map::from_json(j, resp);
			co_return ::grpc::Status::OK;
		})

		class ServiceLogsCallData : public asyncpp::grpc::calldata_interface {
		public:
			ServiceLogsCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_stream{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_stream.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else
						m_stream.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (m_write_in_progress) { return CURL_WRITEFUNC_PAUSE; }
				if (m_is_tty) {
					m_response.mutable_frame()->set_stream_type(0);
					m_response.mutable_frame()->set_payload(data, size);
					m_write_in_progress = true;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
				} else {
					m_write_buf.append(data, size);
					using namespace asyncpp::curl::util;
					if (m_write_buf.size() >= 8) {
						auto stream = get<uint8_t>(m_write_buf.data());
						auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
						if (m_write_buf.size() >= len + 8) {
							m_response.mutable_frame()->set_stream_type(stream);
							m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
							m_write_buf.erase(0, len + 8);
							m_write_in_progress = true;
							m_stream.Write(m_response, ptr_tag<tag::write>(this));
						}
					}
				}
				return size;
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestServiceLogs(&m_ctx, &m_request, &m_stream, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ServiceLogsCallData(m_service, m_cq))->start();
					spdlog::info("Calling inspect on service {} to determine tty mode", m_request.id());
					m_inspect_request.set_id(m_request.id());
					m_stub = DockerService::NewStub(m_service->get_internal_channel());
					m_inspect_reader = m_stub->AsyncServiceInspect(&m_client_context, m_inspect_request, m_cq);
					m_inspect_reader->Finish(&m_inspect_response, &m_inspect_status, ptr_tag<tag::inspect_finish>(this));
					break;
				}
				case tag::inspect_finish: {
					if (!m_inspect_status.ok()) return m_stream.Finish(m_inspect_status, ptr_tag<tag::finish>(this));
					if (!m_inspect_response.has_info() || !m_inspect_response.info().has_spec() || !m_inspect_response.info().spec().has_task_template() ||
						!m_inspect_response.info().spec().task_template().has_container_spec())
						return m_stream.Finish(::grpc::Status(::grpc::StatusCode::INTERNAL, "Missing inspect info"), ptr_tag<tag::finish>(this));
					m_is_tty = m_inspect_response.info().spec().task_template().container_spec().tty();
					spdlog::info("Attaching to {} in {} mode", m_inspect_response.info().id(), m_is_tty ? "tty" : "multiplexed");

					auto url = fmt::format("/v1.41/services/{}/logs?details={}&follow={}&stdout={}&stderr={}&since={}&timestamps={}&tail={}",
										   e(m_inspect_response.info().id()), m_request.details(), m_request.follow(), m_request.stdout_(), m_request.stderr_(),
										   m_request.since(), m_request.timestamps(), m_request.tail() ? std::to_string(m_request.tail()) : "all");
					m_service->get_client().prepare_handle(m_hdl, url);
					m_service->get_client().get_executor().add_handle(m_hdl);
					std::lock_guard lck{m_write_mtx};
					m_response.mutable_info()->CopyFrom(m_inspect_response.info());
					m_write_in_progress = true;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_in_progress == true);
						m_response.clear_info();
						using namespace asyncpp::curl::util;
						if (m_write_buf.size() >= 8) {
							auto stream = get<uint8_t>(m_write_buf.data());
							auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
							if (m_write_buf.size() >= len + 8) {
								m_response.mutable_frame()->set_stream_type(stream);
								m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
								m_write_buf.erase(0, len + 8);
								m_stream.Write(m_response, ptr_tag<tag::write>(this));
								break;
							}
						}
						m_write_in_progress = false;
					}
					m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_RECV); });
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ServiceLogsRequest m_request{};
			ServiceLogsResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncWriter<ServiceLogsResponse> m_stream;
			// Safety checking
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write, read, inspect_finish };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_headers;
			std::mutex m_write_mtx{};
			bool m_write_in_progress{false};
			bool m_is_tty{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};

			// Inspect call data
			std::unique_ptr<DockerService::Stub> m_stub;
			::grpc::ClientContext m_client_context;
			ServiceInspectRequest m_inspect_request;
			ServiceInspectResponse m_inspect_response;
			::grpc::Status m_inspect_status;
			std::unique_ptr<::grpc::ClientAsyncResponseReader<ServiceInspectResponse>> m_inspect_reader;
		};

	} // namespace

	void DockerServiceImpl::start_async_calls_service(::grpc::ServerCompletionQueue* cq) {
		TaskListCallData(this, cq);
		TaskInspectCallData(this, cq);
		(new TaskLogsCallData(this, cq))->start();
		ServiceListCallData(this, cq);
		ServiceCreateCallData(this, cq);
		ServiceInspectCallData(this, cq);
		ServiceDeleteCallData(this, cq);
		ServiceUpdateCallData(this, cq);
		(new ServiceLogsCallData(this, cq))->start();
	}
} // namespace it::thalhammer::docker_gateway