#include "DockerServiceImpl.h"
#include <async_helpers.h>
#include <asyncpp/curl/exception.h>
#include <asyncpp/curl/executor.h>
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <curl/curl.h>
#include <json_map.h>
#include <regex>
#include <spdlog/spdlog.h>

using namespace asyncpp;

namespace it::thalhammer::docker_gateway {
	using namespace ::google::protobuf;

	namespace {

		CALL(ExecCreateCallData, RequestExecCreate, {
			auto res = co_await client.post_json("/v1.41/containers/{}/exec", json_map::to_json(req.config()), e(req.container_id()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		class ExecStartCallData : public asyncpp::grpc::calldata_interface {
		public:
			ExecStartCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_stream{&m_ctx} {
				m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					if (!m_error_buf.empty()) {
						auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
						m_stream.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), ptr_tag<tag::finish>(this));
					} else
						m_stream.Finish(::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
				m_hdl.set_readfunction([this](char* ptr, size_t size) -> size_t {
					if (!m_running) {
						if (!m_initial_formdata.empty()) {
							size = std::min(size, m_initial_formdata.size());
							memcpy(ptr, m_initial_formdata.data(), size);
							m_initial_formdata.erase(0, size);
							if (m_initial_formdata.empty()) {
								m_running = true;
								assert(m_read_in_progress == false);
								m_read_in_progress = true;
								m_stream.Read(&m_request, ptr_tag<tag::read>(this));
							}
							return size;
						}
					}
					if (m_read_in_progress) return CURL_READFUNC_PAUSE;
					auto c = m_request.mutable_frame();
					if (!c->payload().empty()) {
						if (m_is_tty) {
							size = std::min(c->payload().size(), size);
							memcpy(ptr, c->payload().data(), size);
							c->mutable_payload()->erase(0, size);
							return size;
						} else {
							size = std::min(c->payload().size() + 8, size);
							using namespace asyncpp::curl::util;
							size -= 8;
							memset(ptr, 0, 8);
							set<uint8_t>(ptr, c->stream_type());
							set<uint32_t>(ptr + 4, native_to_big<uint32_t>(size));
							ptr += 8;
							memcpy(ptr, c->payload().data(), size);
							c->mutable_payload()->erase(0, size);
							return size + 8;
						}
					} else {
						if (m_read_done) return 0;
						assert(m_read_in_progress == false);
						m_read_in_progress = true;
						m_stream.Read(&m_request, ptr_tag<tag::read>(this));
						return CURL_READFUNC_PAUSE;
					}
				});
			}

			size_t process_read(char* data, size_t size) {
				std::lock_guard lck{m_write_mtx};
				if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
				if (m_write_in_progress) { return CURL_WRITEFUNC_PAUSE; }
				if (m_is_tty) {
					m_response.mutable_frame()->set_stream_type(0);
					m_response.mutable_frame()->set_payload(data, size);
					m_write_in_progress = true;
					m_stream.Write(m_response, ptr_tag<tag::write>(this));
				} else {
					m_write_buf.append(data, size);
					using namespace asyncpp::curl::util;
					if (m_write_buf.size() >= 8) {
						auto stream = get<uint8_t>(m_write_buf.data());
						auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
						if (m_write_buf.size() >= len + 8) {
							m_response.mutable_frame()->set_stream_type(stream);
							m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
							m_write_buf.erase(0, len + 8);
							m_write_in_progress = true;
							m_stream.Write(m_response, ptr_tag<tag::write>(this));
						}
					}
				}
				return size;
			}

			void process_frame(int64_t streamid, std::string_view view) {}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestExecStart(&m_ctx, &m_stream, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new ExecStartCallData(m_service, m_cq))->start();
					m_read_in_progress = true;
					m_stream.Read(&m_request, ptr_tag<tag::read>(this));
					break;
				}
				case tag::read: {
					m_read_in_progress = false;
					if (m_running) {
						if (!ok) m_read_done = true;
						m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_SEND); });
					} else {
						if (!m_request.has_metadata()) {
							return m_stream.Finish(::grpc::Status(::grpc::StatusCode::INVALID_ARGUMENT, "Metadata is missing"), ptr_tag<tag::finish>(this));
						}
						auto& meta = m_request.metadata();
						m_is_tty = meta.tty();
						spdlog::info("Attaching to exec session {} in {} mode", meta.id(), m_is_tty ? "tty" : "multiplexed");

						m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/exec/{}/start", e(meta.id())));
						m_hdl.set_option_long(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
						m_hdl.set_option_bool(CURLOPT_UPLOAD, true);
						m_hdl.set_option_ptr(CURLOPT_CUSTOMREQUEST, "POST");
						m_hdl.set_option_bool(CURLOPT_FORBID_REUSE, true);
						m_hdl.set_option_bool(CURLOPT_FRESH_CONNECT, true);
						m_hdl.set_option_bool(CURLOPT_SUPPRESS_CONNECT_HEADERS, true);
						m_hdl.set_option_long(CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
						m_hdl.set_option_long(CURLOPT_REDIR_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
						auto j = nlohmann::json::object();
						j["Detach"] = meta.detach();
						j["Tty"] = meta.tty();
						m_initial_formdata = j.dump() + "\n";
						m_headers.append("Transfer-Encoding:");
						m_headers.append("Connection: Upgrade");
						m_headers.append("Upgrade: tcp");
						m_headers.append("Expect:");
						m_headers.append(("Content-Length: " + std::to_string(m_initial_formdata.size())).c_str());
						m_headers.append("Content-Type: application/json");
						m_hdl.set_headers(m_headers);
						//m_hdl.set_option_long(CURLOPT_TIMEOUT_MS, 5000);
						m_service->get_client().get_executor().add_handle(m_hdl);
					}
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::write: {
					if (!ok) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					{
						std::lock_guard lck{m_write_mtx};
						assert(m_write_in_progress == true);
						using namespace asyncpp::curl::util;
						if (m_write_buf.size() >= 8) {
							auto stream = get<uint8_t>(m_write_buf.data());
							auto len = native_to_big(get<uint32_t>(m_write_buf.data() + 4));
							if (m_write_buf.size() >= len + 8) {
								m_response.mutable_frame()->set_stream_type(stream);
								m_response.mutable_frame()->set_payload(m_write_buf.data() + 8, len);
								m_write_buf.erase(0, len + 8);
								m_stream.Write(m_response, ptr_tag<tag::write>(this));
								break;
							}
						}
						m_write_in_progress = false;
					}
					m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_RECV); });
					break;
				}
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			ExecStartRequest m_request{};
			ExecStartResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncReaderWriter<ExecStartResponse, ExecStartRequest> m_stream;
			// Safety checking
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, write, read };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_headers;
			std::mutex m_write_mtx{};
			bool m_running{false};
			bool m_write_in_progress{false};
			bool m_read_in_progress{false};
			bool m_read_done{false};
			bool m_is_tty{false};
			std::string m_write_buf{};
			std::string m_last_status{};
			std::string m_error_buf{};
			std::string m_initial_formdata{};
		};

		CALL(ExecResizeCallData, RequestExecResize, {
			co_await client.post_json("/v1.41/exec/{}/resize?w={}&h={}", no_body, e(req.id()), req.width(), req.height());
			co_return ::grpc::Status::OK;
		})

		CALL(ExecInspectCallData, RequestExecInspect, {
			auto res = co_await client.get_json("/v1.41/exec/{}/json", e(req.id()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

	} // namespace

	void DockerServiceImpl::start_async_calls_exec(::grpc::ServerCompletionQueue* cq) {
		ExecCreateCallData(this, cq);
		(new ExecStartCallData(this, cq))->start();
		ExecResizeCallData(this, cq);
		ExecInspectCallData(this, cq);
	}
} // namespace it::thalhammer::docker_gateway