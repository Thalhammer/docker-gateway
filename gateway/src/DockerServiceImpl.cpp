#include "DockerServiceImpl.h"
#include <async_helpers.h>
#include <asyncpp/curl/exception.h>
#include <asyncpp/curl/executor.h>
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <curl/curl.h>
#include <json_map.h>
#include <regex>
#include <spdlog/spdlog.h>

using namespace asyncpp;

namespace it::thalhammer::docker_gateway {
	using namespace ::google::protobuf;

	namespace {

		// ================= Network calls =================

		CALL(NetworkListCallData, RequestNetworkList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/networks?filters={}", e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(NetworkInspectCallData, RequestNetworkInspect, {
			auto scope = (req.scope().empty() ? "" : ("&scope=" + e(req.scope())));
			auto res = co_await client.get_json("/v1.41/networks/{}?verbose={}{}", e(req.id()), req.verbose(), scope);
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(NetworkDeleteCallData, RequestNetworkDelete, {
			auto res = co_await client.delete_json("/v1.41/networks/{}", e(req.id()));
			co_return ::grpc::Status::OK;
		})

		CALL(NetworkCreateCallData, RequestNetworkCreate, {
			auto res = co_await client.post_json("/v1.41/networks/create", json_map::to_json(req));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(NetworkConnectCallData, RequestNetworkConnect, {
			auto res = co_await client.post_json("/v1.41/networks/" + req.id() + "/connect", json_map::to_json(req));
			co_return ::grpc::Status::OK;
		})

		CALL(NetworkDisconnectCallData, RequestNetworkDisconnect, {
			auto res = co_await client.post_json("/v1.41/networks/" + req.id() + "/disconnect", json_map::to_json(req));
			co_return ::grpc::Status::OK;
		})

		CALL(NetworkPruneCallData, RequestNetworkPrune, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.post_json("/v1.41/networks/prune?filters=" + e(filters.dump()), nlohmann::json::object());
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		// ================= Volume calls =================

		CALL(VolumeListCallData, RequestVolumeList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/volumes?filters={}", e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(VolumeCreateCallData, RequestVolumeCreate, {
			auto res = co_await client.post_json("/v1.41/volumes/create", json_map::to_json(req));
			json_map::from_json(res, *resp.mutable_info());
			co_return ::grpc::Status::OK;
		})

		CALL(VolumeInspectCallData, RequestVolumeInspect, {
			auto res = co_await client.get_json("/v1.41/volumes/" + req.name());
			json_map::from_json(res, *resp.mutable_info());
			co_return ::grpc::Status::OK;
		})

		CALL(VolumeDeleteCallData, RequestVolumeDelete, {
			co_await client.delete_json("/v1.41/volumes/" + req.name() + "?force=" + (req.force() ? "true" : "false"));
			co_return ::grpc::Status::OK;
		})

		CALL(VolumePruneCallData, RequestVolumePrune, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.post_json("/v1.41/volumes/prune?filters=" + e(filters.dump()), no_body);
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		// ================= Swarm calls =================

		CALL(SwarmInitCallData, RequestSwarmInit, {
			auto res = co_await client.post_json("/v1.41/swarm/init", json_map::to_json(req));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(SwarmInspectCallData, RequestSwarmInspect, {
			auto res = co_await client.get_json("/v1.41/swarm");
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(SwarmJoinCallData, RequestSwarmJoin, {
			auto res = co_await client.post_json("/v1.41/swarm/join", json_map::to_json(req));
			co_return ::grpc::Status::OK;
		})

		CALL(SwarmUpdateCallData, RequestSwarmUpdate, {
			co_await client.post_json("/v1.41/swarm/update?version={}&rotateWorkerToken={}&rotateManagerToken={}&rotateManagerUnlockKey={}",
									  json_map::to_json(req.spec()), req.version(), req.rotate_worker_token(), req.rotate_manager_token(),
									  req.rotate_manager_unlock_key());
			co_return ::grpc::Status::OK;
		})

		CALL(SwarmUnlockkeyCallData, RequestSwarmUnlockkey, {
			auto res = co_await client.get_json("/v1.41/swarm/unlockkey");
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(SwarmUnlockCallData, RequestSwarmUnlock, {
			co_await client.post_json("/v1.41/swarm/unlock", json_map::to_json(req));
			co_return ::grpc::Status::OK;
		})

		CALL(SwarmLeaveCallData, RequestSwarmLeave, {
			co_await client.post_json("/v1.41/swarm/leave?force={}", no_body, req.force());
			co_return ::grpc::Status::OK;
		})

		// ================= Node calls =================

		CALL(NodeListCallData, RequestNodeList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/nodes?filters=" + e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(NodeInspectCallData, RequestNodeInspect, {
			auto res = co_await client.get_json("/v1.41/nodes/{}", e(req.id()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(NodeDeleteCallData, RequestNodeDelete, {
			co_await client.delete_json("/v1.41/nodes/{}?force={}", e(req.id()), req.force());
			co_return ::grpc::Status::OK;
		})

		CALL(NodeUpdateCallData, RequestNodeUpdate, {
			co_await client.post_json("/v1.41/nodes/{}/update?version={}", json_map::to_json(req.spec()), e(req.id()), req.version());
			co_return ::grpc::Status::OK;
		})

		// ================= Plugin calls =================

		CALL(PluginListCallData, RequestPluginList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/plugins?filters={}", e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(PluginGetPrivilegesCallData, RequestPluginGetPrivileges, {
			auto res = co_await client.get_json("/v1.41/plugins/privileges?remote={}", e(req.remote()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		class PluginPullCallData : public line_response_calldata<DockerServiceImpl, PluginPullRequest, PluginPullResponse> {
		public:
			PluginPullCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : line_response_calldata(service, cq) {}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestPluginPull(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

		private:
			void process_line(std::string_view view) override {
				try {
					auto j = nlohmann::json::parse(view);
					if (j.dump() == m_last_status) return;
					m_last_status = j.dump();
					m_response.Clear();
					json_map::from_json(j, m_response);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
				} catch (...) { spdlog::error("PluginPullCallData({}) failed to parse json: \"{}\"", static_cast<void*>(this), view); }
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new PluginPullCallData(m_service, m_cq))->start();
					nlohmann::json body = nlohmann::json::array();
					for (auto& e : m_request.permissions())
						body.push_back(json_map::to_json(e));
					auto str_body = body.dump();
					m_service->get_client().prepare_handle(m_hdl,
														   fmt::format("/v1.41/plugins/pull?remote={}&name={}", e(m_request.remote()), e(m_request.name())));
					m_hdl.set_readstream(m_post_body);
					m_hdl.set_option_bool(CURLOPT_POST, true);
					m_hdl.set_option_long(CURLOPT_POSTFIELDSIZE, str_body.size());
					m_post_body.str(std::move(str_body));
					m_headers.append("Content-Type: application/json");
					if (m_request.has_registry_auth()) {
						m_headers.append(("X-Registry-Auth: " + DockerServiceImpl::build_registry_auth(m_request.registry_auth())).c_str());
					}
					m_hdl.set_headers(m_headers);
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				default: return line_response_calldata::handle_event(evt, ok);
				}
			}
		};

		CALL(PluginInspectCallData, RequestPluginInspect, {
			auto res = co_await client.get_json("/v1.41/plugins/{}/json", e(req.name()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(PluginDeleteCallData, RequestPluginDelete, {
			co_await client.delete_json("/v1.41/plugins/{}?force={}", e(req.name()), req.force());
			co_return ::grpc::Status::OK;
		})

		CALL(PluginEnableCallData, RequestPluginEnable, {
			co_await client.post_json("/v1.41/plugins/{}/enable?timeout={}", no_body, e(req.name()), req.timeout());
			co_return ::grpc::Status::OK;
		})

		CALL(PluginDisableCallData, RequestPluginDisable, {
			co_await client.post_json("/v1.41/plugins/{}/disable", no_body, e(req.name()));
			co_return ::grpc::Status::OK;
		})

		class PluginUpgradeCallData : public line_response_calldata<DockerServiceImpl, PluginUpgradeRequest, PluginUpgradeResponse> {
		public:
			PluginUpgradeCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : line_response_calldata(service, cq) {}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestPluginUpgrade(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

		private:
			void process_line(std::string_view view) override {
				try {
					auto j = nlohmann::json::parse(view);
					if (j.dump() == m_last_status) return;
					m_last_status = j.dump();
					m_response.Clear();
					json_map::from_json(j, m_response);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
				} catch (...) { spdlog::error("PluginUpgradeCallData({}) failed to parse json: \"{}\"", static_cast<void*>(this), view); }
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new PluginUpgradeCallData(m_service, m_cq))->start();
					nlohmann::json body = nlohmann::json::array();
					for (auto& e : m_request.permissions())
						body.push_back(json_map::to_json(e));
					auto str_body = body.dump();
					m_service->get_client().prepare_handle(m_hdl,
														   fmt::format("/v1.41/plugins/{}/upgrade?remote={}", e(m_request.name()), e(m_request.remote())));
					m_hdl.set_readstream(m_post_body);
					m_hdl.set_option_bool(CURLOPT_POST, true);
					m_hdl.set_option_long(CURLOPT_POSTFIELDSIZE, str_body.size());
					m_post_body.str(std::move(str_body));
					m_headers.append("Content-Type: application/json");
					if (m_request.has_registry_auth()) {
						m_headers.append(("X-Registry-Auth: " + DockerServiceImpl::build_registry_auth(m_request.registry_auth())).c_str());
					}
					m_hdl.set_headers(m_headers);
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				default: return line_response_calldata::handle_event(evt, ok);
				}
			}
		};

		class PluginCreateCallData : public asyncpp::grpc::calldata_interface {
		public:
			PluginCreateCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_reader{&m_ctx} {
				m_hdl.set_donefunction([this](int result) {
					if (m_is_cancelled) return;
					auto str = m_result.str();
					if (!str.empty()) {
						auto j = nlohmann::json::parse(str, nullptr, false);
						if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) str = j["message"];
						m_reader.FinishWithError(map_http_error(m_hdl.get_response_code(), str), ptr_tag<tag::finish>(this));
					} else
						m_reader.Finish(m_response, ::grpc::Status::OK, ptr_tag<tag::finish>(this));
				});
				m_hdl.set_readfunction([this](char* ptr, size_t size) -> size_t {
					auto c = m_request.mutable_chunk();
					if (!c->empty()) {
						size = std::min(c->size(), size);
						memcpy(ptr, c->data(), size);
						c->erase(0, size);
						return size;
					} else {
						if (m_read_done) return 0;
						m_reader.Read(&m_request, ptr_tag<tag::read>(this));
						return CURL_READFUNC_PAUSE;
					}
				});
				m_hdl.set_option_bool(CURLOPT_POST, true);
				m_headers.append("Content-Type: application/x-tar");
				m_hdl.set_headers(m_headers);
				m_hdl.set_writestream(m_result);
			}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestPluginCreate(&m_ctx, &m_reader, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new PluginCreateCallData(m_service, m_cq))->start();
					m_reader.Read(&m_request, ptr_tag<tag::read>(this));
					break;
				}
				case tag::read: {
					if (m_running) {
						if (!ok) m_read_done = true;
						m_service->get_client().get_executor().push([&]() { m_hdl.unpause(CURLPAUSE_SEND); });
					} else {
						m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/plugins/create?name={}", e(m_request.name())));
						m_service->get_client().get_executor().add_handle(m_hdl);
					}
					m_running = true;
					break;
				}
				case tag::notify_done:
					if (m_ctx.IsCancelled()) {
						m_service->get_client().get_executor().remove_handle(m_hdl);
						return delete this;
					}
					break;
				case tag::finish: return delete this;
				default: break;
				}
			}

		private:
			DockerServiceImpl* m_service{nullptr};
			::grpc::ServerCompletionQueue* m_cq{nullptr};
			PluginCreateRequest m_request{};
			PluginCreateResponse m_response{};
			::grpc::ServerContext m_ctx{};
			::grpc::ServerAsyncReader<PluginCreateResponse, PluginCreateRequest> m_reader;
			// Safety checking
			//bool m_finish_called{false};
			std::atomic<bool> m_is_cancelled{false};
			enum class tag { request_start, notify_done, finish, send_initial_metadata, read };

			asyncpp::curl::handle m_hdl{};
			asyncpp::curl::slist m_headers;
			std::mutex m_write_mtx{};
			bool m_running{false};
			bool m_read_done{false};
			std::ostringstream m_result{};
		};

		class PluginPushCallData : public line_response_calldata<DockerServiceImpl, PluginPushRequest, PluginPushResponse> {
		public:
			PluginPushCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : line_response_calldata(service, cq) {}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestPluginPush(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

		private:
			void process_line(std::string_view view) override {
				try {
					auto j = nlohmann::json::parse(view);
					if (j.dump() == m_last_status) return;
					m_last_status = j.dump();
					m_response.Clear();
					json_map::from_json(j, m_response);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
				} catch (...) { spdlog::error("PluginPushCallData({}) failed to parse json: \"{}\"", static_cast<void*>(this), view); }
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new PluginPushCallData(m_service, m_cq))->start();
					m_service->get_client().prepare_handle(m_hdl, fmt::format("/v1.41/plugins/{}/push", e(m_request.name())));
					m_hdl.set_option_bool(CURLOPT_POST, true);
					m_hdl.set_option_long(CURLOPT_POSTFIELDSIZE, 0);
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				default: return line_response_calldata::handle_event(evt, ok);
				}
			}
		};

		CALL(PluginSetCallData, RequestPluginSet, {
			nlohmann::json body = nlohmann::json::array();
			for (auto& e : req.options())
				body.push_back(e);
			co_await client.post_json("/v1.41/plugins/{}/set", body, e(req.name()));
			co_return ::grpc::Status::OK;
		})

		// ================= System calls =================

		CALL(SystemAuthCallData, RequestSystemAuth, {
			auto res = co_await client.post_json(
				"/v1.41/auth", {{"username", req.username()}, {"password", req.password()}, {"email", req.email()}, {"serveraddress", req.server_address()}});
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(SystemInfoCallData, RequestSystemInfo, {
			auto res = co_await client.get_json("/v1.41/info");
			json_map::from_json(res, *resp.mutable_info());
			co_return ::grpc::Status::OK;
		})

		CALL(SystemVersionCallData, RequestSystemVersion, {
			auto res = co_await client.get_json("/v1.41/version");
			json_map::from_json(res, *resp.mutable_version());
			co_return ::grpc::Status::OK;
		})

		class SystemEventsCallData : public line_response_calldata<DockerServiceImpl, SystemEventsRequest, SystemEventsResponse> {
		public:
			// TODO: It might make sense to keep a single connection up to Docker and watch for all events.
			// TODO: We will need to do something like that anyway in the future to invalidate caches.
			SystemEventsCallData(DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) : line_response_calldata(service, cq) {}

			void start() {
				m_ctx.AsyncNotifyWhenDone(ptr_tag<tag::notify_done>(this));
				m_service->RequestSystemEvents(&m_ctx, &m_request, &m_responder, m_cq, m_cq, ptr_tag<tag::request_start>(this));
			}

		private:
			void process_line(std::string_view view) override {
				try {
					auto j = nlohmann::json::parse(view);
					m_response.Clear();
					json_map::from_json(j, m_response);
					m_write_done = false;
					m_responder.Write(m_response, ptr_tag<tag::write>(this));
				} catch (...) { spdlog::error("SystemEventsCallData({}) failed to parse json: \"{}\"", static_cast<void*>(this), view); }
			}

			void handle_event(size_t evt, bool ok) noexcept override {
				switch (static_cast<tag>(evt)) {
				case tag::request_start: {
					if (!ok) return delete this;
					// Initiate a new call
					(new SystemEventsCallData(m_service, m_cq))->start();
					std::string params = "/v1.41/events?filters=" + e(convert_filters(m_request.filters().begin(), m_request.filters().end()).dump());
					if (!m_request.since().empty()) params += "&since=" + e(m_request.since());
					if (!m_request.until().empty()) params += "&until=" + e(m_request.until());
					m_service->get_client().prepare_handle(m_hdl, params);
					m_service->get_client().get_executor().add_handle(m_hdl);
					break;
				}
				default: return line_response_calldata::handle_event(evt, ok);
				}
			}
		};

		CALL(SystemDataUsageCallData, RequestSystemDataUsage, {
			auto res = co_await client.get_json("/v1.41/system/df");
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

	} // namespace

	DockerServiceImpl::DockerServiceImpl() {
		m_docker_url = docker_client::find_docker_address();
		spdlog::info("Using docker at {}", m_docker_url);
		m_docker_client.set_url(m_docker_url);
	}

	void DockerServiceImpl::start_async_calls(::grpc::ServerCompletionQueue* cq) {
		this->start_async_calls_distribution(cq);
		this->start_async_calls_container(cq);
		this->start_async_calls_image(cq);

		NetworkListCallData(this, cq);
		NetworkInspectCallData(this, cq);
		NetworkDeleteCallData(this, cq);
		NetworkCreateCallData(this, cq);
		NetworkConnectCallData(this, cq);
		NetworkDisconnectCallData(this, cq);
		NetworkPruneCallData(this, cq);

		VolumeListCallData(this, cq);
		VolumeCreateCallData(this, cq);
		VolumeInspectCallData(this, cq);
		VolumeDeleteCallData(this, cq);
		VolumePruneCallData(this, cq);

		this->start_async_calls_exec(cq);

		SwarmInitCallData(this, cq);
		SwarmInspectCallData(this, cq);
		SwarmJoinCallData(this, cq);
		SwarmUpdateCallData(this, cq);
		SwarmUnlockkeyCallData(this, cq);
		SwarmUnlockCallData(this, cq);
		SwarmLeaveCallData(this, cq);

		NodeListCallData(this, cq);
		NodeInspectCallData(this, cq);
		NodeDeleteCallData(this, cq);
		NodeUpdateCallData(this, cq);

		this->start_async_calls_service(cq);
		this->start_async_calls_secret(cq);
		this->start_async_calls_config(cq);

		PluginListCallData(this, cq);
		PluginGetPrivilegesCallData(this, cq);
		(new PluginPullCallData(this, cq))->start();
		PluginInspectCallData(this, cq);
		PluginDeleteCallData(this, cq);
		PluginEnableCallData(this, cq);
		PluginDisableCallData(this, cq);
		(new PluginUpgradeCallData(this, cq))->start();
		(new PluginCreateCallData(this, cq))->start();
		(new PluginPushCallData(this, cq))->start();
		PluginSetCallData(this, cq);

		SystemAuthCallData(this, cq);
		SystemInfoCallData(this, cq);
		SystemVersionCallData(this, cq);
		(new SystemEventsCallData(this, cq))->start();
		SystemDataUsageCallData(this, cq);
	}

	std::string DockerServiceImpl::build_registry_auth(const RegistryAuthentication& auth) {
		auto j = nlohmann::json::object();
		if (auth.test_oneof_case() == RegistryAuthentication::kLoginInfo) {
			auto& info = auth.login_info();
			if (!info.username().empty()) j["username"] = info.username();
			if (!info.password().empty()) j["password"] = info.password();
			if (!info.email().empty()) j["email"] = info.email();
			if (!info.server_address().empty()) j["serveraddress"] = info.server_address();
		} else if (auth.test_oneof_case() == RegistryAuthentication::kIdentitytoken) {
			j["identitytoken"] = auth.identitytoken();
		} else
			return "";
		auto res = asyncpp::curl::util::base64_encode(j.dump());
		for (auto& e : res) {
			if (e == '+') e = '-';
			if (e == '/') e = '_';
		}
		res.resize(res.find_last_not_of('='));
		return res;
	}
} // namespace it::thalhammer::docker_gateway