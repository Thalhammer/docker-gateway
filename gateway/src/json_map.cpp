#include <docker_gateway/options.pb.h>
#include <json_map.h>
#include <spdlog/spdlog.h>

namespace it::thalhammer::docker_gateway {
	using namespace ::google::protobuf;

	std::shared_ptr<spdlog::logger> sub_logger(const std::string& name) {
		auto def = spdlog::default_logger();
		auto logger = std::make_shared<spdlog::logger>(name, def->sinks().begin(), def->sinks().end());
		spdlog::register_logger(logger);
		return logger;
	}

	namespace {
		auto g_log = sub_logger("json_map");
	}

	const nlohmann::json* json_map::get_field_loose(const std::string& name, const nlohmann::json& val) {
		for (auto it = val.begin(); it != val.end(); it++) {
			if (it.key().size() == name.size() && strcasecmp(it.key().c_str(), name.c_str()) == 0) return &it.value();
		}
		if (name.find('_') == std::string::npos) return nullptr;
		auto cpy = name;
		auto pos = cpy.find('_');
		while (pos != std::string::npos) {
			cpy.erase(pos, 1);
			pos = cpy.find('_', pos);
		}
		for (auto it = val.begin(); it != val.end(); it++) {
			if (it.key().size() == cpy.size() && strcasecmp(it.key().c_str(), cpy.c_str()) == 0) return &it.value();
		}
		return nullptr;
	}

	template<typename T>
	T get(const nlohmann::json& j, const FieldDescriptor* f) {
		if (j.is_null())
			return 0;
		else if (j.is_number())
			return j;
		else
			throw std::invalid_argument("json is not a number " + f->full_name());
	}

	bool is_full_inline(const Descriptor* d) {
		if (d->field_count() == 1) {
			auto f = d->field(0);
			auto& opts = f->options();
			if (opts.HasExtension(docker_json_inline) && opts.GetExtension(docker_json_inline)) {
				// Inline field
				return true;
			}
		}
		return false;
	}

	void set_field_json(Message& msg, const FieldDescriptor* f, const nlohmann::json& j, bool rep) {
		auto reflect = msg.GetReflection();
		switch (f->cpp_type()) {
		case FieldDescriptor::CPPTYPE_BOOL:
			if (!j.is_boolean()) throw std::invalid_argument("json is not a bool " + f->full_name());
			if (rep)
				reflect->AddBool(&msg, f, j);
			else
				reflect->SetBool(&msg, f, j);
			break;
		case FieldDescriptor::CPPTYPE_INT64:
			if (rep)
				reflect->AddInt64(&msg, f, get<int64_t>(j, f));
			else
				reflect->SetInt64(&msg, f, get<int64_t>(j, f));
			break;
		case FieldDescriptor::CPPTYPE_UINT64:
			if (rep)
				reflect->AddUInt64(&msg, f, get<uint64_t>(j, f));
			else
				reflect->SetUInt64(&msg, f, get<uint64_t>(j, f));
			break;
		case FieldDescriptor::CPPTYPE_INT32:
			if (rep)
				reflect->AddInt32(&msg, f, get<int32_t>(j, f));
			else
				reflect->SetInt32(&msg, f, get<int32_t>(j, f));
			break;
		case FieldDescriptor::CPPTYPE_UINT32:
			if (rep)
				reflect->AddUInt32(&msg, f, get<uint32_t>(j, f));
			else
				reflect->SetUInt32(&msg, f, get<uint32_t>(j, f));
			break;
		case FieldDescriptor::CPPTYPE_FLOAT:
			if (rep)
				reflect->AddFloat(&msg, f, get<float>(j, f));
			else
				reflect->SetFloat(&msg, f, get<float>(j, f));
			break;
		case FieldDescriptor::CPPTYPE_DOUBLE:
			if (rep)
				reflect->AddDouble(&msg, f, get<double>(j, f));
			else
				reflect->SetDouble(&msg, f, get<double>(j, f));
			break;
		case FieldDescriptor::CPPTYPE_STRING:
			if (!j.is_string()) throw std::invalid_argument("json is not a string " + f->full_name());
			if (rep)
				reflect->AddString(&msg, f, j);
			else
				reflect->SetString(&msg, f, j);
			break;
		case FieldDescriptor::CPPTYPE_MESSAGE: {
			if (j.is_null()) break;
			Message* newmsg = nullptr;
			if (rep)
				newmsg = reflect->AddMessage(&msg, f);
			else
				newmsg = reflect->MutableMessage(&msg, f);
			json_map::from_json(j, *newmsg);
			break;
		}
		case FieldDescriptor::CPPTYPE_ENUM: {
			if (j.is_null()) break;
			if (!j.is_number_integer() && !j.is_string()) throw std::invalid_argument("json is not a valid enum " + f->full_name());
			auto e = f->enum_type();
			const EnumValueDescriptor* val = nullptr;
			if (j.is_number())
				val = e->FindValueByNumber(j);
			else
				val = e->FindValueByName(j);
			if (val == nullptr) {
				g_log->warn("Invalid value {} for field {}", j, f->full_name());
				break;
			}
			if (rep)
				reflect->AddEnum(&msg, f, val);
			else
				reflect->SetEnum(&msg, f, val);
			break;
		}
		default: g_log->error("Unhandled field type \"{}\" ({})", f->type_name(), static_cast<int>(f->type())); break;
		}
	}

	void json_map::from_json(const nlohmann::json& j, Message& msg, const FieldDescriptor* f) {
		if (f->is_map()) {
			auto reflect = msg.GetReflection();
			auto map_type = f->message_type();
			auto fkey = map_type->map_key();
			auto fval = map_type->map_value();
			for (auto it = j.begin(); it != j.end(); it++) {
				auto entry = reflect->AddMessage(&msg, f);
				entry->GetReflection()->SetString(entry, fkey, it.key());
				from_json(it.value(), *entry, fval);
			}
			return;
		}
		auto rep = f->is_repeated();
		if (rep) {
			// Null is accepted as the empty list []
			if (j.is_null()) return;
			if (j.is_array()) {
				if (f->cpp_type() == FieldDescriptor::CPPTYPE_MESSAGE && is_full_inline(f->message_type()) && f->message_type()->field(0)->is_repeated()) {
					auto reflect = msg.GetReflection();
					for (auto it = j.begin(); it != j.end(); it++) {
						auto newm = reflect->AddMessage(&msg, f);
						from_json(it.value(), *newm, f->message_type()->field(0));
					}
					return;
				}
				for (auto it = j.begin(); it != j.end(); it++) {
					from_json(it.value(), msg, f);
				}
				return;
			}
		}
		set_field_json(msg, f, j, rep);
	}

	void json_map::from_json(const nlohmann::json& j, Message& msg) {
		auto d = msg.GetDescriptor();
		// Check for single inline fields.
		// This allows using a scalar value as a inline field.
		if (is_full_inline(d)) { return from_json(j, msg, d->field(0)); }

		// Normal object map fields
		if (!j.is_object()) throw std::invalid_argument("json is not a object " + d->full_name());
		for (int i = 0; i < d->field_count(); i++) {
			auto f = d->field(i);
			auto& opts = f->options();
			if (opts.HasExtension(docker_json_inline) && opts.GetExtension(docker_json_inline)) {
				from_json(j, msg, f);
				continue;
			}
			auto& name = opts.HasExtension(docker_json_name) ? opts.GetExtension(docker_json_name) : (f->has_json_name() ? f->json_name() : f->name());
			if (name.empty()) continue;
			auto json = get_field_loose(name, j);
			if (json == nullptr) {
				g_log->warn("Could not find member \"{}\" in json", name);
				continue;
			}
			from_json(*json, msg, f);
		}
	}

	nlohmann::json json_map::to_json(const Message& msg, const FieldDescriptor* f) {
		auto reflect = msg.GetReflection();
		if (f->is_map()) {
			auto map_type = f->message_type();
			auto fkey = map_type->map_key();
			auto fval = map_type->map_value();
			nlohmann::json res = nlohmann::json::object();
			for (int i = 0; i < reflect->FieldSize(msg, f); i++) {
				auto& emsg = reflect->GetRepeatedMessage(msg, f, i);
				res[emsg.GetReflection()->GetString(emsg, fkey)] = to_json(emsg, fval);
			}
			return res;
		}
		auto rep = f->is_repeated();
		if (rep) {
			nlohmann::json res = nlohmann::json::array();
			for (int i = 0; i < reflect->FieldSize(msg, f); i++) {
				switch (f->cpp_type()) {
				case FieldDescriptor::CPPTYPE_BOOL: res.push_back(reflect->GetRepeatedBool(msg, f, i)); break;
				case FieldDescriptor::CPPTYPE_INT64: res.push_back(reflect->GetRepeatedInt64(msg, f, i)); break;
				case FieldDescriptor::CPPTYPE_UINT64: res.push_back(reflect->GetRepeatedUInt64(msg, f, i)); break;
				case FieldDescriptor::CPPTYPE_INT32: res.push_back(reflect->GetRepeatedInt32(msg, f, i)); break;
				case FieldDescriptor::CPPTYPE_UINT32: res.push_back(reflect->GetRepeatedUInt32(msg, f, i)); break;
				case FieldDescriptor::CPPTYPE_FLOAT: res.push_back(reflect->GetRepeatedFloat(msg, f, i)); break;
				case FieldDescriptor::CPPTYPE_DOUBLE: res.push_back(reflect->GetRepeatedDouble(msg, f, i)); break;
				case FieldDescriptor::CPPTYPE_STRING: res.push_back(reflect->GetRepeatedString(msg, f, i)); break;
				case FieldDescriptor::CPPTYPE_MESSAGE: res.push_back(to_json(reflect->GetRepeatedMessage(msg, f, i))); break;
				case FieldDescriptor::CPPTYPE_ENUM: res.push_back(reflect->GetRepeatedEnum(msg, f, i)->name()); break;
				default: g_log->error("Unhandled field type \"{}\" ({})", f->type_name(), static_cast<int>(f->type())); break;
				}
			}
			return res;
		} else {
			switch (f->cpp_type()) {
			case FieldDescriptor::CPPTYPE_BOOL: return reflect->GetBool(msg, f);
			case FieldDescriptor::CPPTYPE_INT64: return reflect->GetInt64(msg, f);
			case FieldDescriptor::CPPTYPE_UINT64: return reflect->GetUInt64(msg, f);
			case FieldDescriptor::CPPTYPE_INT32: return reflect->GetInt32(msg, f);
			case FieldDescriptor::CPPTYPE_UINT32: return reflect->GetUInt32(msg, f);
			case FieldDescriptor::CPPTYPE_FLOAT: return reflect->GetFloat(msg, f);
			case FieldDescriptor::CPPTYPE_DOUBLE: return reflect->GetDouble(msg, f);
			case FieldDescriptor::CPPTYPE_STRING: return reflect->GetString(msg, f);
			case FieldDescriptor::CPPTYPE_MESSAGE: {
				if (!reflect->HasField(msg, f)) return nlohmann::json();
				return to_json(reflect->GetMessage(msg, f));
			}
			case FieldDescriptor::CPPTYPE_ENUM: return reflect->GetEnum(msg, f)->name();
			default: g_log->error("Unhandled field type \"{}\" ({})", f->type_name(), static_cast<int>(f->type())); return nlohmann::json();
			}
		}
	}

	nlohmann::json json_map::to_json(const Message& msg) {
		auto d = msg.GetDescriptor();
		nlohmann::json res = nlohmann::json::object();
		for (int i = 0; i < d->field_count(); i++) {
			auto f = d->field(i);
			auto& opts = f->options();
			if (opts.HasExtension(docker_json_inline) && opts.GetExtension(docker_json_inline)) {
				auto inline_json = to_json(msg, f);
				if (d->field_count() > 1) {
					if (inline_json.is_null())
						continue;
					else if (!inline_json.is_object()) {
						throw std::logic_error("inline fields with siblings need to be objects themselfs");
					} else {
						for (auto it = inline_json.begin(); it != inline_json.end(); it++) {
							res[it.key()] = std::move(it.value());
						}
					}
				} else
					return inline_json;
			}
			auto& name = opts.HasExtension(docker_json_name) ? opts.GetExtension(docker_json_name) : (f->has_json_name() ? f->json_name() : f->name());
			if (name.empty()) continue;
			res[name] = to_json(msg, f);
		}
		return res;
	}
} // namespace it::thalhammer::docker_gateway