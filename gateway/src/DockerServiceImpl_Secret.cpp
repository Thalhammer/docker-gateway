#include "DockerServiceImpl.h"
#include <async_helpers.h>
#include <asyncpp/curl/exception.h>
#include <asyncpp/curl/executor.h>
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <curl/curl.h>
#include <json_map.h>
#include <regex>
#include <spdlog/spdlog.h>

using namespace asyncpp;

namespace it::thalhammer::docker_gateway {
	using namespace ::google::protobuf;

	namespace {

		CALL(SecretListCallData, RequestSecretList, {
			auto filters = convert_filters(req.filters().begin(), req.filters().end());
			auto res = co_await client.get_json("/v1.41/secrets?filters={}", e(filters.dump()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(SecretCreateCallData, RequestSecretCreate, {
			auto res = co_await client.post_json("/v1.41/secrets/create", json_map::to_json(req.spec()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(SecretInspectCallData, RequestSecretInspect, {
			auto res = co_await client.get_json("/v1.41/secrets/{}", e(req.id()));
			json_map::from_json(res, resp);
			co_return ::grpc::Status::OK;
		})

		CALL(SecretDeleteCallData, RequestSecretDelete, {
			co_await client.delete_json("/v1.41/secrets/{}", e(req.id()));
			co_return ::grpc::Status::OK;
		})

		CALL(SecretUpdateCallData, RequestSecretUpdate, {
			co_await client.post_json("/v1.41/secrets/{}/update?version={}", json_map::to_json(req.spec()), e(req.id()), req.version());
			co_return ::grpc::Status::OK;
		})

	} // namespace

	void DockerServiceImpl::start_async_calls_secret(::grpc::ServerCompletionQueue* cq) {
		SecretListCallData(this, cq);
		SecretCreateCallData(this, cq);
		SecretInspectCallData(this, cq);
		SecretDeleteCallData(this, cq);
		SecretUpdateCallData(this, cq);
	}
} // namespace it::thalhammer::docker_gateway