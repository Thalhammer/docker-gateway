#pragma once
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#include <docker_gateway/docker.grpc.pb.h>
#include <grpcpp/grpcpp.h>
#pragma GCC diagnostic pop
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <docker_client.h>

namespace it::thalhammer::docker_gateway {
	class DockerServiceImpl : public DockerService::AsyncService {
		std::string m_docker_url;
		docker_client m_docker_client;
		std::shared_ptr<::grpc::Channel> m_internal_channel;

		void start_async_calls_config(::grpc::ServerCompletionQueue* cq);
		void start_async_calls_distribution(::grpc::ServerCompletionQueue* cq);
		void start_async_calls_container(::grpc::ServerCompletionQueue* cq);
		void start_async_calls_exec(::grpc::ServerCompletionQueue* cq);
		void start_async_calls_image(::grpc::ServerCompletionQueue* cq);
		void start_async_calls_secret(::grpc::ServerCompletionQueue* cq);
		void start_async_calls_service(::grpc::ServerCompletionQueue* cq);

	public:
		static std::string build_registry_auth(const RegistryAuthentication& auth);
		DockerServiceImpl();
		void set_internal_channel(std::shared_ptr<::grpc::Channel> c) { std::atomic_store(&m_internal_channel, c); }
		std::shared_ptr<::grpc::Channel> get_internal_channel() { return std::atomic_load(&m_internal_channel); }
		void start_async_calls(::grpc::ServerCompletionQueue* cq);
		docker_client& get_client() { return m_docker_client; }
		const std::string& get_client_url() { return m_docker_url; }
	};

} // namespace it::thalhammer::docker_gateway