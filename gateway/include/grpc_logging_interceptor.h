#pragma once
#include <memory>

namespace grpc::experimental {
	class ServerInterceptorFactoryInterface;
}
namespace spdlog {
	class logger;
}

namespace it::thalhammer::docker_gateway {
	std::unique_ptr<::grpc::experimental::ServerInterceptorFactoryInterface> create_grpc_logging_interceptor(std::shared_ptr<spdlog::logger> logger = nullptr);
}