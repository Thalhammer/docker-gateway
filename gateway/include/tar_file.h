#pragma once
#include <asyncpp/task.h>
#include <cstddef>

namespace it::thalhammer::docker_gateway::tar {

	inline void set_octal(char* data, size_t len, uint32_t value) {
		for (size_t i = 0; i < len; i++) {
			data[len - i - 1] = '0' + (value % 8);
			value /= 8;
		}
	}

	inline uint32_t get_octal(const char* data, size_t len) {
		uint32_t value = 0;
		for (size_t i = 0; i < len; i++) {
			if (data[i] < '0' || data[i] > '9') continue;
			value *= 8;
			value += data[i] - '0';
		}
		return value;
	}

	inline size_t strlen_n(const char* p, size_t max) {
		size_t len = 0;
		while (max-- && *p++)
			len++;
		return len;
	}

	struct header {
		char filename[100];
		char filemode[8];
		char uid[8];
		char gid[8];
		char filesize[12];
		char last_modified[12];
		char checksum[8];
		char type;
		char linked_file[100];
		char ustar_id[6];
		char ustar_version[2];
		char user_name[32];
		char group_name[32];
		char device_major[8];
		char device_minor[8];
		char filename_prefix[155];
		char reserved[12];

		void set_filemode(uint32_t v) noexcept { set_octal(filemode, sizeof(filemode), v); }
		void set_uid(uint32_t v) noexcept { set_octal(uid, sizeof(uid), v); }
		void set_gid(uint32_t v) noexcept { set_octal(gid, sizeof(gid), v); }
		void set_filesize(uint32_t v) noexcept { set_octal(filesize, sizeof(filesize), v); }
		void set_last_modified(uint32_t v) noexcept { set_octal(last_modified, sizeof(last_modified), v); }
		void set_type(uint32_t v) noexcept { type = std::min(v, uint32_t{6}) + '0'; }
		void set_linked_file(std::string_view v) noexcept { strncpy(linked_file, v.data(), std::min(v.size(), sizeof(linked_file))); }
		void set_user_name(std::string_view v) noexcept { strncpy(user_name, v.data(), std::min(v.size(), sizeof(user_name))); }
		void set_group_name(std::string_view v) noexcept { strncpy(group_name, v.data(), std::min(v.size(), sizeof(group_name))); }
		void set_device_major(uint32_t v) noexcept { set_octal(device_major, sizeof(device_major), v); }
		void set_device_minor(uint32_t v) noexcept { set_octal(device_minor, sizeof(device_minor), v); }
		void set_filename(std::string_view name) noexcept {
			if (name.size() > 100) {
				if (name.size() > 255) name = name.substr(0, 255);
				strncpy(filename, name.substr(name.size() - 100).data(), sizeof(filename));
				strncpy(filename_prefix, name.substr(0, name.size() - 100).data(), std::min(sizeof(filename_prefix), name.size() - 100));
			} else {
				strncpy(filename, name.data(), std::min(sizeof(filename), name.size()));
			}
		}

		uint32_t get_filemode() const noexcept { return get_octal(filemode, sizeof(filemode)); }
		uint32_t get_uid() const noexcept { return get_octal(uid, sizeof(uid)); }
		uint32_t get_gid() const noexcept { return get_octal(gid, sizeof(gid)); }
		uint32_t get_filesize() const noexcept { return get_octal(filesize, sizeof(filesize)); }
		uint32_t get_last_modified() const noexcept { return get_octal(last_modified, sizeof(last_modified)); }
		uint32_t get_type() const noexcept { return type == 0 ? 0 : type - '0'; }
		std::string get_linked_file() const { return std::string{linked_file, strlen_n(linked_file, sizeof(linked_file))}; }
		std::string get_user_name() const { return std::string{user_name, strlen_n(user_name, sizeof(user_name))}; }
		std::string get_group_name() const { return std::string{group_name, strlen_n(group_name, sizeof(group_name))}; }
		uint32_t get_device_major() const noexcept { return get_octal(device_major, sizeof(device_major)); }
		uint32_t get_device_minor() const noexcept { return get_octal(device_minor, sizeof(device_minor)); }
		std::string get_filename() const {
			return std::string{filename_prefix, strlen_n(filename_prefix, sizeof(filename_prefix))} +
				   std::string{filename, strlen_n(filename, sizeof(filename))};
		}

		void finalize() noexcept {
			strncpy(ustar_id, "ustar", sizeof(ustar_id));
			strncpy(ustar_version, "00", sizeof(ustar_version));
			memset(checksum, ' ', sizeof(checksum));
			auto ptr = reinterpret_cast<const uint8_t*>(this);
			uint32_t sum = 0;
			for (size_t i = 0; i < sizeof(header); i++) {
				sum += ptr[i];
			}
			tar::set_octal(checksum, sizeof(checksum), sum);
		}
	};
	static_assert(sizeof(header) == 512);

	struct parser {
		std::function<bool(const header&)> on_header;
		std::function<bool(std::string_view)> on_data;

		size_t parse(std::string_view data) {
			size_t old_size = data.size();
			while (!data.empty()) {
				if (m_state == 0) {
					auto ptr = reinterpret_cast<uint8_t*>(&m_header);
					auto take = std::min(data.size(), size_t{sizeof(header)} - m_byte_count);
					memcpy(ptr + m_byte_count, data.data(), take);
					data.remove_prefix(take);
					m_byte_count += take;
					if (m_byte_count == sizeof(header) && on_header) {
						if (m_header.filename[0] != '\0' || m_header.filename_prefix[0] != '\0')
							if (!on_header(m_header)) break;
						m_byte_count = 0;
						m_read_size = get_octal(m_header.filesize, sizeof(m_header.filesize));
						m_state = 1;
					}
				} else if (m_state == 1) {
					auto take = std::min(data.size(), m_read_size - m_byte_count);
					if (on_data && !on_data(data.substr(0, take))) break;
					m_byte_count += take;
					data.remove_prefix(take);
					if (m_byte_count == m_read_size) { m_state = 2; }
				} else {
					auto rounded = std::max(((m_read_size + 511) / 512) * 512, size_t{0});
					auto take = std::min(data.size(), rounded - m_byte_count);
					m_byte_count += take;
					data.remove_prefix(take);
					if (m_byte_count == rounded) {
						m_state = 0;
						m_byte_count = 0;
					}
				}
			}
			return old_size - data.size();
		}

	private:
		size_t m_byte_count{0};
		size_t m_read_size{0};
		int m_state{0};
		header m_header{};
	};
} // namespace it::thalhammer::docker_gateway::tar