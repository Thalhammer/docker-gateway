#pragma once
#include <asyncpp/curl/executor.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/task.h>
#include <fmt/format.h>
#include <nlohmann/json.hpp>

namespace it::thalhammer::docker_gateway {
	struct docker_request {
		enum class method_type { GET, HEAD, DELETE, POST };
		std::string url;
		asyncpp::curl::slist request_headers{};
		asyncpp::curl::slist response_headers{};
		method_type method{method_type::GET};
		std::string request_body{};
		std::string response_body{};
		int response_code{};

		template<typename TArg1, typename... TArgs>
		static docker_request get(const std::string_view fmt, TArg1&& arg1, TArgs&&... args) {
			return get(fmt::format(fmt, std::move(arg1), std::forward<TArgs>(args)...));
		}
		static docker_request get(const std::string_view url) {
			docker_request req;
			req.url = url;
			req.method = method_type::GET;
			return req;
		}

		template<typename TArg1, typename... TArgs>
		static docker_request head(const std::string_view fmt, TArg1&& arg1, TArgs&&... args) {
			return head(fmt::format(fmt, std::move(arg1), std::forward<TArgs>(args)...));
		}
		static docker_request head(const std::string_view url) {
			docker_request req;
			req.url = url;
			req.method = method_type::HEAD;
			return req;
		}

		template<typename TArg1, typename... TArgs>
		static docker_request delete_(const std::string_view fmt, TArg1&& arg1, TArgs&&... args) {
			return delete_(fmt::format(fmt, std::move(arg1), std::forward<TArgs>(args)...));
		}
		static docker_request delete_(const std::string_view url) {
			docker_request req;
			req.url = url;
			req.method = method_type::DELETE;
			return req;
		}

		template<typename TArg1, typename... TArgs>
		static docker_request post(const std::string_view fmt, TArg1&& arg1, TArgs&&... args) {
			return post(fmt::format(fmt, std::move(arg1), std::forward<TArgs>(args)...));
		}
		static docker_request post(const std::string_view url) {
			docker_request req;
			req.url = url;
			req.method = method_type::POST;
			return req;
		}

		template<typename TArg1, typename... TArgs>
		static docker_request post_json(const nlohmann::json& body, const std::string_view fmt, TArg1&& arg1, TArgs&&... args) {
			return post_json(body, fmt::format(fmt, std::move(arg1), std::forward<TArgs>(args)...));
		}
		static docker_request post_json(const nlohmann::json& body, const std::string_view url) {
			auto req = post(url);
			if (body.is_discarded()) return req;
			req.request_body = body.dump();
			req.request_headers.append("Content-Type: application/json");
			return req;
		}
	};

	class docker_client {
		asyncpp::curl::executor m_executor;
		std::string m_url;

	public:
		docker_client();
		void set_url(const std::string& url);

		asyncpp::curl::executor& get_executor() noexcept { return m_executor; }

		void prepare_handle(asyncpp::curl::handle& hdl, const std::string& url) const;

		asyncpp::task<nlohmann::json> get_json(const std::string url);
		template<typename... TArgs>
		asyncpp::task<nlohmann::json> get_json(const std::string_view url, TArgs&&... args) {
			return get_json(fmt::format(url, std::forward<TArgs>(args)...));
		}
		asyncpp::task<std::multimap<std::string, std::string>> head(const std::string url);
		template<typename... TArgs>
		asyncpp::task<std::multimap<std::string, std::string>> head(const std::string_view url, TArgs&&... args) {
			return head(fmt::format(url, std::forward<TArgs>(args)...));
		}
		asyncpp::task<nlohmann::json> delete_json(const std::string url);
		template<typename... TArgs>
		asyncpp::task<nlohmann::json> delete_json(const std::string_view url, TArgs&&... args) {
			return delete_json(fmt::format(url, std::forward<TArgs>(args)...));
		}
		asyncpp::task<nlohmann::json> post_json(const std::string url, const nlohmann::json& body);
		asyncpp::task<nlohmann::json> post_json(const std::string url);
		template<typename... TArgs>
		asyncpp::task<nlohmann::json> post_json(const std::string_view url, const nlohmann::json& body, TArgs&&... args) {
			return post_json(fmt::format(url, std::forward<TArgs>(args)...), body);
		}

		asyncpp::task<void> execute(docker_request& req);

		static nlohmann::json check_return(asyncpp::curl::handle& hdl, const std::string& msg);

		static std::string find_docker_address();
	};
} // namespace it::thalhammer::docker_gateway