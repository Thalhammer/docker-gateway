#pragma once
#include <asyncpp/curl/handle.h>
#include <asyncpp/curl/slist.h>
#include <asyncpp/curl/util.h>
#include <asyncpp/grpc/traits.h>
#include <asyncpp/grpc/unary_task.h>
#include <curl/curl.h>
#include <nlohmann/json.hpp>
#include <sstream>

namespace it::thalhammer::docker_gateway {

	inline std::string e(const std::string& x) { return asyncpp::curl::util::escape(x); }

	template<typename TIt>
	inline nlohmann::json convert_filters(TIt begin, TIt end) {
		nlohmann::json res = nlohmann::json::object();
		for (auto it = begin; it != end; it++) {
			const std::string& str = *it;
			auto pos = str.find_first_of('=');
			if (pos == std::string::npos) continue;
			auto key = str.substr(0, pos);
			auto val = str.substr(pos + 1);
			res[key][val] = true;
		}
		return res;
	}

	inline ::grpc::Status map_http_error(int code, const std::string& message) {
		switch (code) {
		case 400: return ::grpc::Status(::grpc::StatusCode::INVALID_ARGUMENT, message); break;
		case 401: return ::grpc::Status(::grpc::StatusCode::UNAUTHENTICATED, message); break;
		case 403: return ::grpc::Status(::grpc::StatusCode::PERMISSION_DENIED, message); break;
		case 404: return ::grpc::Status(::grpc::StatusCode::NOT_FOUND, message); break;
		case 409: return ::grpc::Status(::grpc::StatusCode::FAILED_PRECONDITION, message); break;
		default:
		case 500: return ::grpc::Status(::grpc::StatusCode::INTERNAL, message); break;
		case 503: return ::grpc::Status(::grpc::StatusCode::FAILED_PRECONDITION, message); break;
		}
	}

#define CALL(Name, Method, Content)                                                                                                                            \
	asyncpp::grpc::unary_task<typename asyncpp::grpc::method_traits<decltype(&DockerService::AsyncService::Method)>::response_type> Name(                      \
		DockerServiceImpl* service, ::grpc::ServerCompletionQueue* cq) {                                                                                       \
		auto [ok, req, resp, ctx] = co_await asyncpp::grpc::start(&DockerService::AsyncService::Method, service, cq);                                          \
		if (!ok) co_return ::grpc::Status::CANCELLED;                                                                                                          \
		Name(service, cq);                                                                                                                                     \
		auto& client = service->get_client();                                                                                                                  \
                                                                                                                                                               \
		Content                                                                                                                                                \
	}

	constexpr static auto no_body = nlohmann::json::value_t::discarded;

	template<typename TService, typename TRequest, typename TResponse>
	class line_response_calldata : public asyncpp::grpc::calldata_interface {
	protected:
		line_response_calldata(TService* service, ::grpc::ServerCompletionQueue* cq) : m_service{service}, m_cq{cq}, m_responder{&m_ctx} {
			m_hdl.set_writefunction([this](char* ptr, size_t size) -> size_t { return process_read(ptr, size); });
			m_hdl.set_donefunction([this](int) {
				if (m_is_cancelled) return;
				if (!m_error_buf.empty()) {
					auto j = nlohmann::json::parse(m_error_buf, nullptr, false);
					if (!j.is_discarded() && j.contains("message") && j["message"].is_string()) m_error_buf = j["message"];
					m_responder.Finish(map_http_error(m_hdl.get_response_code(), m_error_buf), asyncpp::ptr_tag<tag::finish>(this));
				} else
					m_responder.Finish(::grpc::Status::OK, asyncpp::ptr_tag<tag::finish>(this));
			});
		}

		size_t process_read(char* data, size_t size) {
			std::lock_guard lck{m_write_mtx};
			if (!m_error_buf.empty() || m_hdl.get_response_code() / 100 != 2) { m_error_buf.append(data, size); }
			if (!m_write_done) {
				m_write_curl_paused = true;
				return CURL_WRITEFUNC_PAUSE;
			}
			m_write_buf.append(data, size);
			auto pos = m_write_buf.find_first_of("\r\n");
			if (pos != std::string::npos) {
				process_line(std::string_view{m_write_buf.c_str(), pos});
				auto end = m_write_buf.find_first_not_of("\r\n", pos);
				m_write_buf.erase(0, end);
			}
			return size;
		}

		virtual void process_line(std::string_view view) = 0;

		void handle_event(size_t evt, bool ok) noexcept override {
			switch (static_cast<tag>(evt)) {
			case tag::notify_done:
				if (m_ctx.IsCancelled()) {
					m_service->get_client().get_executor().remove_handle(m_hdl);
					return delete this;
				}
				break;
			case tag::write: {
				if (!ok) {
					m_service->get_client().get_executor().remove_handle(m_hdl);
					return delete this;
				}
				{
					std::lock_guard lck{m_write_mtx};
					assert(m_write_done == false);
					m_write_done = true;
				}
				if (m_write_curl_paused) {
					m_service->get_client().get_executor().push([&]() {
						m_write_curl_paused = false;
						m_hdl.unpause(CURLPAUSE_SEND);
					});
				}
				break;
			}
			case tag::finish: return delete this;
			default: break;
			}
		}

		TService* m_service{nullptr};
		::grpc::ServerCompletionQueue* m_cq{nullptr};
		TRequest m_request{};
		TResponse m_response{};
		::grpc::ServerContext m_ctx{};
		::grpc::ServerAsyncWriter<TResponse> m_responder;
		// Safety checking
		//bool m_finish_called{false};
		std::atomic<bool> m_is_cancelled{false};
		enum class tag { request_start, notify_done, finish, send_initial_metadata, write };

		asyncpp::curl::handle m_hdl{};
		asyncpp::curl::slist m_headers;
		std::istringstream m_post_body;
		std::mutex m_write_mtx{};
		bool m_write_done{true};
		bool m_write_curl_paused{false};
		std::string m_write_buf{};
		std::string m_last_status{};
		std::string m_error_buf{};
	};

} // namespace it::thalhammer::docker_gateway