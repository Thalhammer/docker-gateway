#pragma once
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>

namespace google::protobuf {
	class Message;
	class FieldDescriptor;
} // namespace google::protobuf

namespace it::thalhammer::docker_gateway {

	class json_map {
		static const nlohmann::json* get_field_loose(const std::string& name, const nlohmann::json& val);

	public:
		static void from_json(const nlohmann::json& j, ::google::protobuf::Message& msg, const ::google::protobuf::FieldDescriptor* f);
		static void from_json(const nlohmann::json& j, ::google::protobuf::Message& msg);
		static nlohmann::json to_json(const ::google::protobuf::Message& msg);
		static nlohmann::json to_json(const ::google::protobuf::Message& msg, const ::google::protobuf::FieldDescriptor* f);
	};
} // namespace it::thalhammer::docker_gateway