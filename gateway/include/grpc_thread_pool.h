#pragma once
#include <asyncpp/grpc/calldata_interface.h>
#include <asyncpp/ptr_tag.h>
#include <grpcpp/server_builder.h>
#include <memory>
#include <thread>
#include <vector>

namespace it::thalhammer::docker_gateway {
	class grpc_thread_pool {
		std::vector<std::thread> m_threads;
		std::vector<std::unique_ptr<::grpc::ServerCompletionQueue>> m_cqs;
		std::shared_ptr<::grpc::Server> m_server{nullptr};

	public:
		std::shared_ptr<::grpc::Server> start(::grpc::ServerBuilder& builder, size_t nthread = std::thread::hardware_concurrency()) {
			m_threads.resize(nthread);
			m_cqs.resize(nthread);
			for (size_t i = 0; i < nthread; i++) {
				m_cqs[i] = builder.AddCompletionQueue(true);
				m_threads[i] = std::thread{[i, this]() {
					while (true) {
						void* tag = nullptr;
						bool ok = false;
						auto res = m_cqs[i]->Next(&tag, &ok);
						if (!res) break;
						auto [ptr, evt] = ::asyncpp::ptr_untag<::asyncpp::grpc::calldata_interface>(tag);
						ptr->handle_event(evt, ok);
					}
				}};
			}
			m_server = builder.BuildAndStart();
			return m_server;
		}

		void stop() {
			if (!m_server) return;
			m_server->Shutdown();
			for (auto& e : m_cqs)
				e->Shutdown();
			for (auto& e : m_threads)
				if (e.joinable()) e.join();
			m_cqs.clear();
			m_server = nullptr;
			m_threads.clear();
		}

		template<typename TFunc>
		void apply_cq(TFunc&& fn) {
			for (auto& e : m_cqs)
				fn(e.get());
		}

		~grpc_thread_pool() { stop(); }
	};
} // namespace it::thalhammer::docker_gateway