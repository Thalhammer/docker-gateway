#include <docker_gateway/docker.grpc.pb.h>
#include <fstream>
#include <grpcpp/grpcpp.h>
#include <spdlog/spdlog.h>
#include <termios.h>
#include <thread>
#include <unistd.h>

using namespace it::thalhammer::docker_gateway;

#define APP 7

#if APP == 1
int main(int, const char**) {
	auto channel = ::grpc::CreateChannel("127.0.0.1:50051", ::grpc::InsecureChannelCredentials());
	auto stub = DockerService::NewStub(channel);
	::grpc::ClientContext ctx;
	PluginCreateResponse resp;
	auto reader = stub->PluginCreate(&ctx, &resp);
	std::ifstream file{"test", std::ios::binary};
	if (!file) spdlog::error("failed to open file");
	while (file) {
		PluginCreateRequest req;
		req.set_name("my_plugin");
		auto c = req.mutable_chunk();
		c->resize(512 * 1024);
		auto s = file.read(c->data(), c->size()).gcount();
		c->resize(s);
		reader->Write(req);
		spdlog::info("write {}", req.chunk().size());
	}
	reader->WritesDone();
	auto status = reader->Finish();
	spdlog::info("finished {} {}", status.error_code(), status.error_message());
	return 0;
}
#elif APP == 2
int main(int, const char**) {
	auto channel = ::grpc::CreateChannel("127.0.0.1:50051", ::grpc::InsecureChannelCredentials());
	auto stub = DockerService::NewStub(channel);
	::grpc::ClientContext ctx;
	auto reader = stub->ImageLoad(&ctx);
	std::ifstream file{"test", std::ios::binary};
	std::thread th([&]() {
		ImageLoadResponse resp;
		while (reader->Read(&resp)) {
			spdlog::info("got resp");
		}
	});
	if (!file) spdlog::error("failed to open file");
	while (file) {
		ImageLoadRequest req;
		auto c = req.mutable_chunk();
		c->resize(512 * 1024);
		auto s = file.read(c->data(), c->size()).gcount();
		c->resize(s);
		reader->Write(req);
		spdlog::info("write {}", req.chunk().size());
	}
	reader->WritesDone();
	auto status = reader->Finish();
	if (th.joinable()) th.join();
	spdlog::info("finished {} {}", status.error_code(), status.error_message());
	return 0;
}
#elif APP == 3
int main(int, const char** argv) {
	auto channel = ::grpc::CreateChannel("127.0.0.1:50051", ::grpc::InsecureChannelCredentials());
	auto stub = DockerService::NewStub(channel);
	ImageGetAllRequest req;
	req.add_names("ubuntu:latest");
	req.add_names("node:16");
	req.set_chunk_size(std::stoul(argv[1]) * 1024);
	::grpc::ClientContext ctx;
	auto reader = stub->ImageGetAll(&ctx, req);
	ImageGetAllResponse resp;
	auto start = std::chrono::steady_clock::now();
	size_t sum = 0;
	while (reader->Read(&resp)) {
		sum += resp.chunk().size();
	}
	auto status = reader->Finish();
	auto dur = std::chrono::steady_clock::now() - start;
	spdlog::info("finished {} {}", status.error_code(), status.error_message());
	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(dur);
	spdlog::info("{} byte in {} ms ({}kB/s)", sum, ms.count(), sum / ms.count());
	return 0;
}
#elif APP == 4
int main(int, const char**) {
	auto channel = ::grpc::CreateChannel("127.0.0.1:50051", ::grpc::InsecureChannelCredentials());
	auto stub = DockerService::NewStub(channel);
	::grpc::ClientContext ctx;
	auto reader = stub->ImageBuild(&ctx);
	std::ifstream file{"test", std::ios::binary};
	std::thread th([&]() {
		ImageBuildResponse resp;
		while (reader->Read(&resp)) {
			spdlog::info("{}", resp.DebugString());
		}
	});
	if (!file) spdlog::error("failed to open file");
	ImageBuildRequest req;
	req.mutable_metadata()->set_nocache(true);
	req.mutable_metadata()->set_pull(true);
	while (file) {
		auto c = req.mutable_chunk();
		c->resize(512 * 1024);
		auto s = file.read(c->data(), c->size()).gcount();
		c->resize(s);
		reader->Write(req);
		req.clear_metadata();
		spdlog::info("write {}", req.chunk().size());
	}
	reader->WritesDone();
	auto status = reader->Finish();
	if (th.joinable()) th.join();
	spdlog::info("finished {} {}", status.error_code(), status.error_message());
	return 0;
}
#elif APP == 5
int main(int, const char**) {
	auto channel = ::grpc::CreateChannel("127.0.0.1:50051", ::grpc::InsecureChannelCredentials());
	auto stub = DockerService::NewStub(channel);
	::grpc::ClientContext ctx;
	ContainerArchivePutResponse resp;
	auto reader = stub->ContainerArchivePut(&ctx, &resp);
	std::ifstream file{"test_archive.tar", std::ios::binary};
	if (!file) spdlog::error("failed to open file");
	ContainerArchivePutRequest req;
	req.set_id("1c1a0ce35e1c");
	req.set_path("/");
	req.set_no_overwrite_dir_non_dir(true);
	req.set_copy_uidgid(true);
	while (file) {
		auto c = req.mutable_chunk();
		c->resize(512 * 1024);
		auto s = file.read(c->data(), c->size()).gcount();
		c->resize(s);
		reader->Write(req);
		spdlog::info("write {}", req.chunk().size());
	}
	reader->WritesDone();
	auto status = reader->Finish();
	spdlog::info("finished {} {}", status.error_code(), status.error_message());
	return 0;
}
#elif APP == 6
int main(int argc, const char** argv) {
	if (argc < 2) {
		std::cerr << argv[0] << " <container>" << std::endl;
		return -1;
	}
	static struct termios term_default;
	tcgetattr(STDIN_FILENO, &term_default);
	atexit([]() { tcsetattr(STDIN_FILENO, TCSAFLUSH, &term_default); });
	{
		auto raw = term_default;
		raw.c_lflag &= ~(ECHO | ICANON);
		tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
	}
	std::cout.sync_with_stdio(true);
	std::cerr.sync_with_stdio(true);
	auto channel = ::grpc::CreateChannel("127.0.0.1:50051", ::grpc::InsecureChannelCredentials());
	auto stub = DockerService::NewStub(channel);
	::grpc::ClientContext ctx;
	auto reader = stub->ContainerAttach(&ctx);
	std::thread th([&]() {
		ContainerAttachResponse resp;
		while (reader->Read(&resp)) {
			if (resp.has_info()) { spdlog::info("Attached to container {} name={}", resp.info().id(), resp.info().name()); }
			if (!resp.has_frame()) continue;
			[[maybe_unused]] auto r = write(resp.frame().stream_type(), resp.frame().payload().data(), resp.frame().payload().size());
		}
	});
	ContainerAttachRequest req;
	req.mutable_metadata()->set_id(argv[1]);
	req.mutable_metadata()->set_detach_keys("ctrl-p,ctrl-q");
	req.mutable_metadata()->set_logs(true);
	req.mutable_metadata()->set_stdin(true);
	req.mutable_metadata()->set_stream(true);
	req.mutable_metadata()->set_stdout(true);
	req.mutable_metadata()->set_stderr(true);
	auto ok = reader->Write(req);
	req.clear_metadata();
	while (ok) {
		char c;
		if (read(STDIN_FILENO, &c, 1) != 1) break;
		auto f = req.mutable_frame();
		f->set_payload(&c, 1);
		f->set_stream_type(0);
		ok = reader->Write(req);
	}
	reader->WritesDone();
	auto status = reader->Finish();
	if (th.joinable()) th.join();
	spdlog::info("finished {} {}", status.error_code(), status.error_message());
	return 0;
}
#elif APP == 7
int main(int argc, const char** argv) {
	if (argc < 2) {
		std::cerr << argv[0] << " <container>" << std::endl;
		return -1;
	}
	static struct termios term_default;
	tcgetattr(STDIN_FILENO, &term_default);
	atexit([]() { tcsetattr(STDIN_FILENO, TCSAFLUSH, &term_default); });
	{
		auto raw = term_default;
		raw.c_lflag &= ~(ECHO | ICANON);
		tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
	}
	std::cout.sync_with_stdio(true);
	std::cerr.sync_with_stdio(true);
	auto channel = ::grpc::CreateChannel("127.0.0.1:50051", ::grpc::InsecureChannelCredentials());
	auto stub = DockerService::NewStub(channel);
	ExecStartRequest execreq;
	{
		::grpc::ClientContext ctx;
		ExecCreateRequest req;
		req.set_container_id(argv[1]);
		req.mutable_config()->set_attach_stdin(true);
		req.mutable_config()->set_attach_stdout(true);
		req.mutable_config()->set_attach_stderr(true);
		req.mutable_config()->set_tty(true);
		req.mutable_config()->add_cmd("ps");
		req.mutable_config()->add_cmd("aux");
		ExecCreateResponse resp;
		auto status = stub->ExecCreate(&ctx, req, &resp);
		if (!status.ok()) {
			spdlog::error("Failed to create exec session {}", status.error_message());
			return -1;
		}
		execreq.mutable_metadata()->set_id(resp.id());
	}
	::grpc::ClientContext ctx;
	auto reader = stub->ExecStart(&ctx);
	std::thread th([&]() {
		ExecStartResponse resp;
		while (reader->Read(&resp)) {
			if (!resp.has_frame()) continue;
			[[maybe_unused]] auto r = write(resp.frame().stream_type(), resp.frame().payload().data(), resp.frame().payload().size());
		}
	});
	execreq.mutable_metadata()->set_tty(true);
	execreq.mutable_metadata()->set_detach(false);
	auto ok = reader->Write(execreq);
	execreq.clear_metadata();
	while (ok) {
		char c;
		if (read(STDIN_FILENO, &c, 1) != 1) break;
		auto f = execreq.mutable_frame();
		f->set_payload(&c, 1);
		f->set_stream_type(0);
		ok = reader->Write(execreq);
	}
	reader->WritesDone();
	auto status = reader->Finish();
	if (th.joinable()) th.join();
	spdlog::info("finished {} {}", status.error_code(), status.error_message());
	return 0;
}
#endif