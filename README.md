# GRPC API gateway for docker
This gateway provides a grpc api for docker. It does so by providing a grpc server that in turn calls into the docker api to get information.

## Why ?
GRPC is (imho) way superior to a plain http api. It is in most cases faster, is easier to maintain and most importantly allows automatic generation of client
code for virtualy any language out there. In addition it supports streaming in both directions natively, unlike the hack docker currently uses.

In addition to the plain wrapping, this gateway might add additional features in the future, like access controls, multitenant, etc.

## Versioning
Unlike docker itself, which uses a version prefix on its apis, this gateway uses the options built into protobuf for versioning and aims to provide a polyfill if
the version of the docker service it is talking to does not support a specific feature. It is however recommended to keep docker as recent as possible.

Docker-Gateway implements and uses the latest docker api at the time of writing (v1.41). Details about api changes on dockers side can be found
[here](https://docs.docker.com/engine/api/version-history/).

## Support
Since we want to maintain compatibility with multiple docker versions as well as use grpc naming conventions, we can't simply autogenerate the code from the docker api docs.
You can check which docker side apis are supported in the below table.

- [X] Containers
- [X] Images
- [X] Networks
- [X] Volumes
- [X] Exec
- [X] Swarm
- [X] Nodes
- [X] Services
- [X] Tasks
- [X] Secrets
- [X] Configs
- [X] Plugins
- [X] System
- [X] Distribution
- [ ] Session
